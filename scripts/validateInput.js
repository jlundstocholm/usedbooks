function validateLogin(strEmail,strPassword) {
	var strMessage = '';
	var bInputValid = true;
	if(strEmail.length == 0 || strPassword.length == 0) {
	
		strMessage = 'Du skal angive b�de en emailadresse og et password for at logge ind.';
		if(strEmail.length == 0 && strPassword.length > 0) {
			bInputValid = false;
			document.forms[1].strEmail.focus();
		}
		if(strEmail.length > 0 && strPassword.length == 0) {
			bInputValid = false;
			document.forms[1].strPassword.focus();
		}
		if(strEmail.length == 0 && strPassword.length == 0) {
			bInputValid = false;
			document.forms[1].strEmail.focus();
		}
		alert(strMessage);
	}
	return bInputValid;
}

function promptToDelete(strURI,strCheckSum,bDoDelete) {
	if(confirm('Er du sikker p�, at du vil slette denne bog ?\n\nSkulle du senere ombestemme dig, s� kan\ndu altid g�re denne bog tilg�ngelig igen via\nbogadministration.')) {
		var strRedirectURI = '/admin/bookadmin/bookdelete/?id=' + strURI + '&dodelete=' + bDoDelete + '&checksum=' + strCheckSum;
		location.href = strRedirectURI;
		return false;
	}
	else 
	{
		return false;
	}
}

function checkPassword() {
	var strMessage = '';
	var bInputValid = true;
	return true;
	if (document.forms[2].PasswordOriginal.value.length == 0) {
		strMessage = 'Du skal indtaste dit nuv�rende password, som du evt har modtaget pr. email.';
		alert(strMessage);
		document.forms[2].PasswordOriginal.focus();
		return false;
	}
	if (document.forms[2].PasswordNew.value.length == 0 || document.forms[2].PasswordConfirm.value.length == 0) {
		strMessage = 'Du skal b�de v�lge et nyt password og bekr�fte det i formularen.';
		alert(strMessage);
		document.forms[2].PasswordNew.focus();
		return false;
	}
	
	if (document.forms[2].PasswordNew.value != document.forms[2].PasswordConfirm.value) {
		strMessage = 'Dit nye password og bekr�ftelsen af det er ikke ens.';
		alert(strMessage);
		document.forms[2].PasswordNew.focus();
		return false;
	}
	return bInputValid;
}

//Opdateret med strMobile 2003-02-07af Morten
function checkContactFormular(strName,strPhone,strMobile,strEmail,strMessage) {
	if(strName.length == 0) {
		alert('Du skal angive dit navn for at kunne skrive til kontaktpersonen.');
		document.forms.fContact.strName.focus();
		return false;
	}
	
	if(strMessage.length == 0) {
		alert('Du skal skrive en besked i beskedfeltet for at kunne kontakt brugeren.');
		document.forms.fContact.strMessage.focus();
		return false;
	}
	//strMobile tilf�jet herunder
	if(strPhone.length == 0 && strMobile.length == 0 && strEmail.length == 0) {
		if(!alert('Du har ikke angivet hverken telefonnumre eller en emailadresse. G� tilbage og udfyld mindst et af disse felter.')) {
			document.forms.fContact.strEmail.focus();
			return false;
		}
	}
}

function checkNewUser(strFirstName,strLastName,strEmail,strPhone,strMobile,strPassword,strPasswordConf,bOffJa,bOffNej) {
	var bValidInput = true;
	if(strFirstName.length == 0) {
		alert('Du skal angive dit fornavn.');
		document.forms.createUser.fornavn.focus();
		return false;
	}
	
	if(strLastName.length == 0) {
		alert('Du skal angive dit efternavn.');
		document.forms.createUser.efternavn.focus();
		return false;
	}
	
	var EmailCheck  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; //RegExp der matcher struktur p� emailadresse
	if (!EmailCheck.test(strEmail)) {
		alert('Din emailadresse er ikke korrekt.');
		document.forms.createUser.email.focus();
		return false;
	}
	
	var MobileCheck = /\d{8}/; //RegExp der matcher 8 cifre
	if (strMobile.length > 0) {
		if (!MobileCheck.test(strMobile)) {
			alert('Dit mobilnummer skal best� af 8 tal.');
			document.forms.createUser.mobil.focus();
			return false;
		}
	}
	
	if (strPassword.length < 4) {
		alert('Dit password skal v�re p� mindst 4 tegn.');
		document.forms.createUser.password.focus();
		return false;
	}
	
	if (strPasswordConf.length < 4) {
		alert('Du skal bekr�fte dit password.');
		document.forms.createUser.password2.focus();
		return false;
	}
	
	if (strPassword != strPasswordConf) {
		alert('B�de dit password og bekr�ftelsen af det skal v�re ens.');
		document.forms.createUser.password.focus();
		return false;
	}
	
	//var PasswordCheckLetter = /.*[a-zA-Z].*[a-zA-Z].*/; //RegExp der matcher en streng med mindst 2 bogstaver
	//var PasswordCheckNumber = /.*[0-9].*[0-9].*/; //RegExp der matcher en streng med mindst 2 tal
	//if (!(PasswordCheckLetter.test(strPassword) && PasswordCheckLetter.test(strPassword))) {
	//	alert('Dit password skal best� af mindst to bogstaver og to tal.');
	//	document.forms.createUser.password.focus();
	//	return false;
	//}
	
	
}

function validateEditProfile(strFirstName,strLastName,strEmail,lngMobile,strPasswordNew,strPasswordNewConf) {
	
	var bValidInput = true;
	if(strFirstName.length == 0) {
		alert('Du skal angive dit fornavn.');
		document.forms.formEdit.fornavn.focus();
		return false;
	}
	
	if(strLastName.length == 0) {
		alert('Du skal angive dit efternavn.');
		document.forms.formEdit.efternavn.focus();
		return false;
	}
	
	var EmailCheck  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; //RegExp der matcher struktur p� emailadresse
	if (!EmailCheck.test(strEmail)) {
		alert('Din emailadresse er ikke korrekt.');
		document.forms.formEdit.email.focus();
		return false;
	}
	
	var MobileCheck = /\d{8}/; //RegExp der matcher 8 cifre
	if (lngMobile.length > 0) {
		if (!MobileCheck.test(lngMobile)) {
			alert('Dit mobilnummer skal best� af 8 tal.');
			document.forms.formEdit.mobil.focus();
			return false;
		}
	}
	
	if (strPasswordNew.length != 0 || strPasswordNewConf.length != 0) {
	
		if (strPasswordNew.length < 4) {
			alert('Dit nye password er for kort.\n\nHvis du �nsker at �ndre dit password, s� skal du indtaste det i formularen. Du skal herefter bekr�fte det. B�de dit nye password og bekr�ftelsen af det skal v�re p� mindst 4 tegn');
			document.forms.formEdit.nytpassword.focus();
			return false;
		}

		if (strPasswordNewConf.length < 4) {
			alert('Bekr�ftelsen af dit nye password er for kort.\n\nHvis du �nsker at �ndre dit password, s� skal du indtaste det i formularen. Du skal herefter bekr�fte det. B�de dit nye password og bekr�ftelsen af det skal v�re p� mindst 4 tegn');
			document.forms.formEdit.nytpassword2.focus();
			return false;
		}

		if (strPasswordNew != strPasswordNewConf) {
			alert('B�de dit password og bekr�ftelsen af det skal v�re ens.');
			document.forms.formEdit.nytpassword.focus();
			return false;
		}
		//nedenst�ende bruges ikke l�ngere
		//var PasswordCheckLetter = /.*[a-zA-Z].*[a-zA-Z].*/; //RegExp der matcher en streng med mindst 2 bogstaver
		//var PasswordCheckNumber = /.*[0-9].*[0-9].*/; //RegExp der matcher en streng med mindst 2 tal
		//if (!(PasswordCheckLetter.test(strPasswordNew) && PasswordCheckLetter.test(strPasswordNew))) {
		//	alert('Dit password skal best� af mindst to bogstaver og to tal.');
		//	document.forms.formEdit.nytpassword.focus();
		//	return false;
		//}	
	}
}

function CheckNewBook(oDoc) {
	var Error = '';
	var validData = true;
	
	// check title
	if (oDoc.Title.value.length == 0) {
		Error += '\n- Du skal indtaste en titel for bogen.';
		validData = false;
	}
	
	// check price
	var PriceCheck = /^\d+$/; //RegExp der matcher 8 cifre
	if (!PriceCheck.test(oDoc.price.value)) {
		 Error += '\n- Du skal indtaste en pris (i hele kroner).';
		validData = false;
	}
	
	// check ISBN
	//var ISBN10digit =  /^\d{10}$/;
	//var ISBN9digitX =  /^\d{9}[a-z]$/i;
	//var ISBN13digit =  /^\d{13}$/;
	//var ISBN12digitX = /^\d{12}[a-z]$/i;
	var pattern = /^\d{9}(\d{3})?[\da-z]$/i;
	
	var isbn = oDoc.isbn.value;
	if (isbn.length > 0) {
		if (!pattern.test(isbn)) {
			Error += '\n- ISBN er indstastet forkert. Se beskrivelsen ved feltet.';
			validData = false;
		}
	}
	
	if (!validData) {
		alert('Tilf�j/ret f�lgende:\n' + Error);
		return validData;
	}
	return validData;
}
