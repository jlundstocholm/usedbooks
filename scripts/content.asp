<h1 class="content-indent">Om usedbooks.dk</h1>
    <div class="content-indent">
	usedbooks.dk er et website der formidler kontakten mellem s�lger og k�ber af brugte b�ger blandt DTU-studerende. 
	Denne gratis tjeneste har k�rt siden primo 2003, og brugen af websitet er steget til nu at have over 1.000 brugere, og 
	disse brugere har handlet b�ger til en v�rdi der langt har overg�et vores forventninger. Hvert semester benytter en 
	stadig stigende antal studerende tjenesten, og oms�tningen stiger markant for hvert semester.<br />
	<br />
	Vi h�ber at vores brugere f�r sparet mange penge p� k�b af studieb�ger, og f�r tjent penge p� at s�lge de gamle 
	studieb�ger.<br />
	<br />  
	Morten Ovi og Jesper Stocholm<br />

		<% if bShowNewsList then %>
			<h2>Nyheder</h2>
			<table width="560" class="result" id="Table1">
			<tr class="result">
				<td>Dato</td><td>Overskrift</td><td>&nbsp;</td>
			</tr>
			<%
			do while not oRsNews.EOF 
			if strClass = "lgray" then strClass = "dgray" else strClass = "lgray" end if
			%>
			<tr cellspacing="0" class="<% = strClass %>">
				<td width="130" class="<% = strClass %>">
					<% = FormatDateTime(oRsNews("dtCreateDate"),vbLongDate) %></td>
				<td width="365" class="<% = strClass %>"><% = Server.HTMLEncode(oRsNews("strSubject")) %></td>
				<td width="65" class="<% = strClass %>"><a class="<% = strClass %>" href="/news?newsid=<% = Server.URLEncode(oRsNews("strGuid"))%>&amp;checksum=<% = md5(application("initVEctor") & oRsNews("strGuid"))%>"><img src="/images/detaljer.jpg" width="60" height="17" alt="Se detaljer om denne nyhed"/></a></td>
			</tr>
			<%
				oRsNews.MoveNext
				loop
			%>
			</table>
		<% end if %>
		<h2>Seneste b�ger</h2>
		<table width="560" class="result">
		<tr class="result">
			<td>Titel</td><td>Prisid�</td><td>Kursus</td><td>Detaljer</td>
		</tr>
		<% dim strClass ' as string
		do while not oRsTop.EOF 
		if strClass = "lgray" then strClass = "dgray" else strClass = "lgray" end if
		%>
		<tr cellspacing="0" class="<% = strClass %>">
			<td class="<% = strClass %>"><img src="/images/<% = trim(oRsTop("buyORsell")) %>.jpg" width="15" height="15" alt="Denne bog <% if trim(oRsTop("buyORsell")) = "k" then Response.Write "�nskes k�bt" else Response.Write "er til salg" end if %>" />
				<% if Len(oRsTop("strBookTitle")) < 50 then Response.Write Server.HTMLEncode(oRsTop("strBookTitle")) else Response.Write Server.HTMLEncode(Left(oRsTop("strBookTitle"),47)) & " ..." end if %></td>
			<td class="<% = strClass %>"><% = Replace(FormatCurrency(oRsTop("intPrice"),2)," ","&nbsp;") %></td>
			<td class="<% = strClass %>" style="text-align: center;"><% if not isNull(oRsTop("strCourseId")) then Response.Write "<a class=""" & strClass & """ href=""/search?courses=" & oRsTop("strCourseId") & """>" & oRsTop("strCourseId") & "</a>" end if %></td>
			<td class="<% = strClass %>"><form action="/books/default.asp?id=<% = Server.URLEncode(oRsTop("bookId")) %>&amp;checksum=<% = md5(application("initVEctor") & oRsTop("bookId"))%>" method="post"><input src="/images/detaljer.jpg" type="image"/></form></td>
		</tr>
		<%
			oRsTop.MoveNext
			loop
		%>
		</table>
	</div>
	<br />
</div>