<h1>Vilk�r</h1>
	<div class="content-indent">
	Vi har best�bt os p� at g�re dette website s� brugervenligt som muligt. Dette indeb�rer at 
	vi ikke har gjort brug af teknologier som man ikke kan forvente at de fleste af brugerne 
	har. Ydermere er der fokuseret meget p� hvilke oplysninger der opsamles n�r du bes�ger 
	dette site (privacy).
	</div>
<h2 class="content-indent">Privacy</h2>
    <h3 class="content-indent">Generelt</h3>
    <div class="content-indent">
		Ved bes�g p� dette website gemmes informationer om hvilke sider der hentes, hvorn�r
		de hentes samt data om IP-adresse, operativsystem og browser. Disse gemmes for
		at give os mulighed for at optimere vores service og opdage eventuelle fejlagtige links
		og flaskehalse i vores system.<br/>
		Ingen af de oplysninger der bliver indsamlet bliver videregivet (eller solgt) til 3. part.
    </div>
<h3 class="content-indent">Login</h3>
	<div class="content-indent">
		For at kunne levere en ordentlig service n�r en bruger logger ind p� websitet s� 
		g�r vi brug af midlertidlige cookies. Denne cookie bliver slettet s� snart brugeren 
		forlader websitet (eller lukker browseren) og bliver kun brugt for at undg�, at du som
		bruger af vores service ikke skal indtaste et password hele tiden.<br/>
		<br/>
		Vores annonc�rer kan anvende cookies, der vil blive fors�gt sat p� din computer, mens
		du anvender vores website. Skulle du anvende software, der blokerer for reklamer eller 
		cookies, vil det ikke have nogen betydning for din brug af [usedbooks.dk].
	</div>
<h3 class="content-indent">Brugeroplysninger</h3>
	<div class="content-indent">
	For at registrere sig som bruger p� [usedbooks.dk], kr�ver det kun et minimum af oplysninger, dvs. 
	navn, emailadresse og password. Passwords gemmes krypterede, s�ledes at vi ikke kan se vores brugers 
	passwords.<br/>
	<br/>
	Hvis du blot vil k�be en bog der er sat til salg, eller �nsker at s�lge en bog til en bruger, der 
	�nsker at k�be bogen, s� kr�ver det ingen registrering hos os. Vi formidler blot kontakten til den 
	registrerede bruger - alts� den der har sat en bog til k�b/salg.
	</div>
<h3 class="content-indent">Misbrug af brugerkonto og/eller bogopslag</h3>
	<div class="content-indent">
	[usedbooks.dk] forbeholder sig retten til at slette brugerkonti og/eller bogopslag, s�fremt de sk�nnes ulovlige, 
	st�dende, kr�nkende eller tilsvarende. Der vil aldrig blive rettet i oplysningerne om en bruger og/eller et 
	bogopslag, og brugeren vil altid f� besked via email om overtr�delsen. [usedbooks.dk] forbeholder sig retten til 
	uden grund ikke at oprette bestemte brugere og/eller bogopslag.
	</div>
<h3 class="content-indent">Emailadresser og spam</h3>
	<div class="content-indent">
	I brugerprofilen er det muligt at angive om man vil modtage nyhedsbreve fra [usedbooks.dk] i form af drift-emails og generelle emails. [usedbooks.dk] bestr�ber sig p� at sende s� f� meddelelser ud som muligt.
	</div>
<% '<h3 class="content-indent">Spam</h3>
	'<div class="content-indent">
	'For at beskytte brugernes emailadresse mod spam er det ikke muligt at f� oplyst emailadressen p� en bruger 
	'f�r man foretager en s�gning. Da dette heldigvis ikke er muligt for en spam-h�ster, mener vi at vi har sikret vores
	'brugere tilstr�kkeligt mod spam.
	'</div>
	%>
<h2 class="content-indent">Krav til browseren</h2>
    <div class="content-indent">
    Der bliver p� dette website brugt
    <ul>
    <li>XHTML 1.0 valideret HTML-kode</li>
    <li>CSS2</li>
    <li>Midlertidige cookies ved login</li>
    <li>JavaScript, s�fremt browseren underst�tter dette. Underst�ttet JavaScript ikke, vil brugeren stadig kunne bruge
    websitet, dog under mere beskedne vilk�r.</li>
    </ul>
    Bruger man en af f�lgende browsere (eller senere udgaver) vil dette website virke med alle dets funktioner: Internet Explorer 6.0, Netscape Navigator 7.0 eller Mozilla 1.0.<br/><br/>
    Vil du teste om din browser underst�tter JavaScript? <br/><br/>
    <form action="">
		<div>
			<input type="button" value="Test af JavaScript" onclick="alert('Din browser underst�tter javascript.');"/><br/>
			<br/>
			(Der kommer en tekstboks frem hvis det virker)
		</div>
	</form>
    </div>