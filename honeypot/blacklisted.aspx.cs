using System;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace usedbooks.honeypot
{
	/// <summary>
	/// Summary description for blacklisted.
	/// </summary>
	public class blacklisted : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txbUserData;

		private void Page_Load(object sender, System.EventArgs e)
		{
			txbUserData.Columns = 50;
			txbUserData.Rows = 6;
			txbUserData.Wrap = false;
			txbUserData.TextMode = TextBoxMode.MultiLine;

			StringBuilder sb = new StringBuilder();
			sb.Append("Tidspunkt: " + DateTime.Now.ToString() + "\n");
			sb.Append("IP: " + Request.ServerVariables["REMOTE_ADDR"].ToString() + "\n");
			sb.Append("Port: " + Request.ServerVariables["SERVER_PORT"].ToString() + "\n");
			sb.Append("Metode: " + Request.ServerVariables["REQUEST_METHOD"].ToString() + "\n");
			sb.Append("User-agent: " + Request.ServerVariables["HTTP_USER_AGENT"].ToString());
			txbUserData.Text = sb.ToString();

			Session.Abandon();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
