using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using usedbooks;

namespace usedbooks.honeypot
{
	/// <summary>
	/// Summary description for _default.
	/// </summary>
	public class trapRobots : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txbUserData;
		private void Page_Load(object sender, System.EventArgs e)
		{
			txbUserData.Columns = 50;
			txbUserData.Rows = 6;
			txbUserData.Wrap = false;
			txbUserData.TextMode = TextBoxMode.MultiLine;

			StringBuilder sb = new StringBuilder();
			sb.Append("Tidspunkt: " + DateTime.Now.ToString() + "\n");
			sb.Append("IP: " + Request.ServerVariables["REMOTE_ADDR"].ToString() + "\n");
			sb.Append("Port: " + Request.ServerVariables["SERVER_PORT"].ToString() + "\n");
			sb.Append("Metode: " + Request.ServerVariables["REQUEST_METHOD"].ToString() + "\n");
			sb.Append("User-agent: " + Request.ServerVariables["HTTP_USER_AGENT"].ToString());
			txbUserData.Text = sb.ToString();

			tools ubTools = new tools();


			if (!ubTools.isInWhiteList(Request.ServerVariables["REMOTE_ADDR"].ToString(),Request.ServerVariables["HTTP_USER_AGENT"].ToString()))
			{
				ubTools.UpdateBanList(Request.ServerVariables["REMOTE_ADDR"].ToString(),Convert.ToInt32(Request.ServerVariables["SERVER_PORT"]),Request.ServerVariables["REQUEST_METHOD"].ToString(),Request.ServerVariables["HTTP_USER_AGENT"].ToString());
			}
		}

//		private void UpdateBanList(string IP, int Port, string Method, string UserAgent) 
//		{
//			dbmySql oDb = new dbmySql();
//			string strSql = "INSERT INTO ubdk_honeypot (dtDate,dtTime,RemoteIP,Port,Method,UserAgent) VALUES (CURRENT_DATE,CURRENT_TIME,'" + IP + "'," + Port.ToString() + ",'" + Method + "','" + UserAgent + "')";
//			oDb.insertData(strSql);
//		}

//		private bool isInWhiteList(string RemoteIP,string UserAgent) 
//		{
//			string MD5Hash = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(Application["initVector"].ToString() + RemoteIP + UserAgent,"MD5");
//
//			string Jesper = Application["initVector"].ToString() + "130.225.90.69" + "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705)";
//			string JesperMD5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(Jesper,"MD5");
//				//JesperMD5 = 448382E83E7A2DC8971C5C62480199FB
//
//			string Morten = Application["initVector"].ToString() + "130.225.71.227" + "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705)";
//			string MortenMD5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(Morten,"MD5");
//				//MortenMD5 = 638EB8684C69E5CD7DF36CEBF70A220A
//
//			if (String.Compare(MD5Hash,JesperMD5,true) == 0 || String.Compare(MD5Hash,MortenMD5,true) == 0) 
//			{
//				return true;
//			}
//			else 
//			{
//				return false;
//			}
//		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
