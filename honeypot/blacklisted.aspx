<%@ Page language="c#" src="blacklisted.aspx.cs" AutoEventWireup="false" Inherits="usedbooks.honeypot.blacklisted" %>
<!doctype html 
     public "-//w3c//dtd xhtml 1.0 strict//en"
     "http://www.w3.org/tr/xhtml1/dtd/xhtml1-strict.dtd">
<html>
	<head>
		<title>[usedbooks.dk] - Honeypot Blacklisting</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0" />
		<link href="/styles/default.css" type="text/css" rel="stylesheet" />

	</head>
	<body >
		<table class="headertable" width="760" id="Table1">
			<tr>
				<td class="logoelement">
					<img src="/images/usedbooks.dk.jpg" alt="usedbooks.dk" height="84" width="337" />
				</td>
				<td align="right">
					<form method="get" action="/search" id="Form1">
						<div>
							<input disabled="disabled" size="30" class="white" value="" type="text" name="query" id="Text1"/><input  type="submit" value="S�g" disabled="disabled"/><br />
							<span>ISBN</span><input  type="radio" name="search" value="isbn" id="Radio1"/>&nbsp;
							<span>Titel</span><input  type="radio" name="search" value="title" id="Radio2"/>&nbsp;
							<span>Forfatter</span><input  type="radio" name="search" value="author" id="Radio3"/>&nbsp; 
							Alle felter<input checked="checked" type="radio" name="search" value="all" id="Radio4"/><br/>
							
						</div>
					</form>
				</td>
			</tr>
		</table>
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					
					<form action="" method="post" >
						<table width="100%" class="box" id="Table3">
							<tr>
								<td>
									E-mail:<br />
									<input disabled="disabled" class="white" type="text" size="10" id="strEmail" name="strEmail" /><br />
									Password:<br />
									<input disabled="disabled" class="white" type="password" value="" size="10" id="strPassword" name="strPassword" /> 
									<input disabled="disabled" type="submit" value="Login" /><br />
									Ny bruger<br/>
									Glemt password?
								</td>
							</tr>
						</table>
					</form>
					<br />
					<table width="100%" class="box">
						<tr>
							<td>Kontakt</td>
						</tr>
						<tr>
							<td>Vilk�r</td>
						</tr>
						<tr>
							<td>Nyheder</td>
						</tr>
						<tr>
							<td>Hj�lp til s�gning</td>
						</tr>
						<tr>
							<td>Om usedbooks.dk</td>
						</tr>
					</table>
					<br />
				</td>
				<td class="leftline">
					<h1>[usedbooks.dk] - blacklist</h1>
					<div class="content-indent">
						Du er blevet sendt til denne side, da du er blevet blacklistet
						af vores system. Dette skyldes, at du/din maskine har opf�rt
						sig som en robot, der har fors�gt at h�ste emailadresser p�
						vores website.
					</div>
					<h2 class="content-indent">Er dette en fejl ?</h2>
					<div class="content-indent">
						Det er naturligvis muligt, at vi tager fejl. I dette tilf�lde
						beder vi dig skrive til os p� adressen <a href="&#x6d;&#97;&#x69;&#108;&#x74;&#111;&#x3a;&#x73;&#112;&#x61;&#109;&#x40;&#117;&#x73;&#101;&#x64;&#98;&#x6f;&#111;&#x6b;&#115;&#x2e;&#100;&#x6b;">&#x73;&#112;&#x61;&#109;&#x40;&#117;&#x73;&#101;&#x64;&#98;&#x6f;&#111;&#x6b;&#115;&#x2e;&#100;&#x6b;</a>
						Vi beder dig om at medsende data herunder, s� vi kan slette dig
						hurtigst muligt.<br/>
						<br/>
						Hvis vi har taget fejl, s� beklager vi det meget, men vi har
						v�ret n�dt til at stramme op, da vores service tidligere
						har v�ret bes�gt af &quot;onde robotter&quot;. Vi h�ber, at du
						har forst�else herfor.
					</div>
					<h2 class="content-indent">Data om dig</h2>
					<div class="content-indent">
						Vi har noteret os f�lgende data om dig:
						<form runat="server" id="formX">
							<asp:textbox id="txbUserData" runat="server"/>
						</form>
					</div>				
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
