<%@ Page language="c#" src="default.aspx.cs" AutoEventWireup="false" Inherits="usedbooks.honeypot.trapRobots" %>
<!doctype html 
     public "-//w3c//dtd xhtml 1.0 strict//en"
     "http://www.w3.org/tr/xhtml1/dtd/xhtml1-strict.dtd">
<html>
	<head>
		<title>[usedbooks.dk] - Honeypot trap</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0" />
		<link href="/styles/" type="text/css" rel="stylesheet" />

	</head>
	<body >
		<table class="headertable" width="760" id="Table1">
			<tr>
				<td class="logoelement">
					<img src="/images/usedbooks.dk.jpg" alt="usedbooks.dk" height="84" width="337" />
				</td>
				<td align="right">
					<form method="get" action="/search" id="Form1">
						<div>
							<input disabled="disabled" size="30" class="white" value="" type="text" name="query" id="Text1"/><input  type="submit" value="S�g" disabled="disabled"/><br />
							<span>ISBN</span><input  type="radio" name="search" value="isbn" id="Radio1"/>&nbsp;
							<span>Titel</span><input  type="radio" name="search" value="title" id="Radio2"/>&nbsp;
							<span>Forfatter</span><input  type="radio" name="search" value="author" id="Radio3"/>&nbsp; 
							Alle felter<input checked="checked" type="radio" name="search" value="all" id="Radio4"/><br/>
							
						</div>
					</form>
				</td>
			</tr>
		</table>
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					
					<form action="" method="post" >
						<table width="100%" class="box" id="Table3">
							<tr>
								<td>
									E-mail:<br />
									<input disabled="disabled" class="white" type="text" size="10" id="strEmail" name="strEmail" /><br />
									Password:<br />
									<input disabled="disabled" class="white" type="password" value="" size="10" id="strPassword" name="strPassword" /> 
									<input disabled="disabled" type="submit" value="Login" /><br />
									Ny bruger<br/>
									Glemt password?
								</td>
							</tr>
						</table>
					</form>
					<br />
					<table width="100%" class="box">
						<tr>
							<td>Kontakt</td>
						</tr>
						<tr>
							<td>Vilk�r</td>
						</tr>
						<tr>
							<td>Nyheder</td>
						</tr>
						<tr>
							<td>Hj�lp til s�gning</td>
						</tr>
						<tr>
							<td>Om usedbooks.dk</td>
						</tr>
					</table>
					<br />
				</td>
				<td class="leftline">
					<h1>[usedbooks.dk]</h1>
					<div class="content-indent">
					Du er blevet viderestillet til denne del af [usedbooks.dk], da vi
					desv�rre er blevet tvunget til at stramme op omkring de bes�gende
					robotter, der gennems�ger vores website for emailadresser - for 
					derefter at sende spam til vores brugere.
					</div>
					<h2 class="content-indent">Er dette en fejl ?</h2>
					<div class="content-indent">
						Det er naturligvis ogs� muligt, at vi tager fejl, og at du
						er en ganske almindelig nysgerrig bruger, der har siddet og
						leget lidt p� vores website. Hvis dette er tilf�ldet, s�
						skriv blot til os p� adressen <a href="&#x6d;&#97;&#x69;&#108;&#x74;&#111;&#x3a;&#x73;&#112;&#x61;&#109;&#x40;&#117;&#x73;&#101;&#x64;&#98;&#x6f;&#111;&#x6b;&#115;&#x2e;&#100;&#x6b;">&#x73;&#112;&#x61;&#109;&#x40;&#117;&#x73;&#101;&#x64;&#98;&#x6f;&#111;&#x6b;&#115;&#x2e;&#100;&#x6b;</a>.
						Du m� meget gerne medsende data nedenfor i emailen til os,
						s� sletter vi dig fra vores &quot;Black-list&quot; med det 
						samme.<br/>
						<br/>
						Hvis vi har taget fejl, s� beklager vi det meget, men vi har
						v�ret n�dt til at stramme op, da vores service tidligere
						har v�ret bes�gt af &quot;onde robotter&quot;. Vi h�ber, at du
						har forst�else herfor.
					</div>
					<h2 class="content-indent">Konsekvenser</h2>
					<div class="content-indent">
						Sandsynligheden taler for, at du er en robot, der ikke
						respekterer specifikationerne i &quot;Robots Exclusion Standard&quot;.
						Denne standard specificerer, at robotter b�r holde sig fra specifikke
						dele af websites. At du er her tyder p�, at du ikke respekterer dette.<br/>
						<br/>
						At du har hentet denne side medf�rer nogle konsekvenser for dig.
						Vi har noteret os hvilken browser du bruger samt hvilken 
						IP-adresse du anvender. Disse data er blevet tilf�jet vores 
						&quot;Black-list&quot;, og det vil ikke l�ngere v�re muligt for 
						dig at anvende vores website, n�r vi opdaterer vores liste.
					</div>
					<h2 class="content-indent">Data</h2>
					<div class="content-indent">
						Vi har noteret os f�lgende data om dig.
						<form runat="server" id="formX">
							<asp:TextBox id="txbUserData" runat="server"/>
						</form>
					</div>					
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
