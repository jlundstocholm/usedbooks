using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace usedbooks.admin
{
	/// <summary>
	/// Summary description for login.
	/// </summary>
	public class login : System.Web.UI.Page
	{	
		protected System.Web.UI.WebControls.Label Label1;
		private void Page_Load(object sender, System.EventArgs e)
		{
			string strChecksum;
			string strChecksumTemp;
			try
			{
				strChecksumTemp = Application["initVector"].ToString()+Server.UrlDecode(Request.QueryString["strFirstName"].ToString())+Server.UrlDecode(Request.QueryString["strLastName"].ToString())+Server.UrlDecode(Request.QueryString["strEmail"].ToString())+Server.UrlDecode(Request.QueryString["userid"].ToString())+Server.UrlDecode(Request.QueryString["intRole"].ToString())+Server.UrlDecode(Convert.ToBoolean(Request.QueryString["logonValid"].ToString()).ToString());
				strChecksum = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(strChecksumTemp,"MD5");
				if (Server.UrlDecode(Request.QueryString["checksum"].ToString()).Equals(strChecksum))
				{
					Session["strFirstName"]	=	Server.UrlDecode(Request.QueryString["strFirstName"].ToString());
					Session["strLastName"]	=	Server.UrlDecode(Request.QueryString["strLastName"].ToString());
					Session["strEmail"]		=	Server.UrlDecode(Request.QueryString["strEmail"].ToString());
					Session["userId"]		=	Server.UrlDecode(Request.QueryString["userid"].ToString());
					Session["intRole"]		=	Server.UrlDecode(Request.QueryString["intRole"].ToString());
					Session["logonValid"]	=	Convert.ToBoolean(Server.UrlDecode(Request.QueryString["logonValid"].ToString()));
					
					if (Session["intRole"] == "1") {
						Response.Redirect("/admin/tools");
					}
					else {
						Response.Redirect("/admin/bookadmin");
					}
				}
			}
			catch(Exception exp)
			{
				Label1.Text = exp.Message;
				//Response.Redirect("/");
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
