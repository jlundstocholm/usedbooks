<% if not bValidInput then %>
<h1>Der skete en fejl</h1>
<div class="content-indent"><% = strMessage %></div>
<br/>
<div class="content-indent">Brug din browsers &quot;Tilbage-knap&quot; for at rette det.</div>
<% else %>
<h1>Din konto er blevet aktiveret !</h1>
<div class="content-indent">
	<% = strMessage %>
</div>
<% end if %>