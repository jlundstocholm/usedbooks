<%@Language="VBScript"%>
<%
option explicit

if not Session("logonValid") then
	Response.Redirect("/")
end if

%>
<!-- #include virtual="/utilities/md5.asp" -->  
<%
Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.CacheControl = "no-cache"
Response.AddHeader "pragma","no-cache"

%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
' Check om alle felter er korrekt udfyldt (ogs� evt. nyt password).

dim strMessage ' as string

	
dim strFirstName,strLastName,strEmail,strPhone,strPassword,strPasswordNy,strPasswordNyConf ' as string
dim lngMobile ' as long
dim intPublic,intEmail ' as integer
dim boolChangeEmail ' as boolean

strFirstName = Replace(Request.Form("fornavn"),"'","''")
strLastName = Replace(Request.Form("efternavn"),"'","''")
strEmail = Replace(Request.Form("email"),"'","''")
strPhone = Replace(Request.Form("telefon"),"'","''")
lngMobile = Replace(Request.Form("mobil")," ","")
strPasswordNy = Replace(Request.Form("nytpassword"),"'","''")
strPasswordNyConf = Replace(Request.Form("nytpassword2"),"'","''")
intPublic = Request.Form("offentlig")
intEmail = Request.Form("letters")

boolChangeEmail = false

public function validateInput
	validateInput = false
	
	dim bValidInput

	if Len(strFirstName) = 0 then
		strMessage = "Du skal angive dit fornavn."
		bValidInput = false
		exit function
	end if
	
	if Len(strLastName) = 0 then
		strMessage = "Du skal angive dit efternavn."
		bValidInput = false
		exit function
	end if
	
	if Len(strEmail) = 0 then
		strMessage = "Du skal angive en emailadresse. " &_
					"V�r opm�rksom p�, at denne nye emailadresse fungerer som dit " &_
					"login-id p� usedbooks.dk . Derfor er det vigtigt, at du v�lger " &_
					"en emailadresse, som du kan huske."
		bValidInput = false
		exit function
	end if
	
	if not (Len(lngMobile) = 0 OR Len(lngMobile) = 8) then
		strMessage = "Dit mobilnummer skal best� af 8 cifre eller ikke v�re angivet."
		bValidInput = false
		exit function
	end if
	
	if Len(lngMobile) <> 0 then
		if not (isNumeric(lngMobile) AND Len(lngMobile) = 8) then
			strMessage = "Dit mobilnummer skal best� af 8 cifre, dvs ingen bogstaver, " &_
						"bindestreger, punktummer eller lignende."
			bValidInput = false
			exit function
		end if
	end if
	
	if strPasswordNy <> strPasswordNyConf then
		strMessage = "Dit nye password samt bekr�ftelsen af det skal v�re ens."
		bValidInput = false
		exit function
	end if
	
	if Len(strPasswordNy) > 0 OR Len(strPasswordNyConf) > 0 then
		if Len(strPasswordNy) < 4 OR Len(strPasswordNyConf) < 4 then
			strMessage = "Dit nye password skal v�re mindst 4 tegn langt."
			bValidInput = false
			exit function
		end if
	end if
	
	if not isNumeric(intPublic) then
		strMessage = "Du kan kun v�lge imellem at have dine oplysninger offentlige " &_
					"eller undlade det."
		bValidInput = false
		exit function
	end if
	
	if not (intPublic = 1 OR intPublic = 0) then
		strMessage = "Du kan kun v�lge imellem at have dine oplysninger offentlige " &_
					"eller undlade det."
		bValidInput = false
		exit function
	end if
	
	if not isNumeric(intEmail) then
		strMessage = "Du skal tage stilling til hvilke nyhedsbreve du vil modtage."
		bValidInput = false
		exit function
	end if
	
	if not (intEmail >= 0 AND intEmail < 4) then
		strMessage = "Du skal tage stilling til hvilke nyhedsbreve du vil modtage."
		bValidInput = false
		exit function
	end if
	
	
	if Session("strEmail") <> strEmail then
		dim oRsCheckEmail ' as ADODB.Recordset
		set oRsCheckEmail = Server.CreateObject("ADODB.Recordset")
		dim strSQLCheck ' as string
		strSqlCheck = "SELECT COUNT(strEmail) As CountOfUsers FROM ubdk_users WHERE strEmail = '" & strEmail & "'"
		with orsCheckEmail
			.ActiveConnection =	oCon
			.CursorLocation =	3 ' adUseclient
			.CursorType =		0 ' adOpenForwardOnly
			.LockType =			1 ' adLockReadOnly
			.Source =			strSqlCheck
			.Open
		end with
	
		if CInt(oRsCheckEmail("CountOfUsers")) <> 0 then
			strMessage = "Din angivne emailadresse findes allerede i databasen. " & _
						"Hvis du er sikker p�, at emailadressen er din, s� kan du f� " &_
						"tilsendt et password til profilen ved at bruge " &_
						"linket ""Glemt password?"" i menuen til venstre."
			oRsCheckEmail.Close
			set oRsCheckEmail = nothing
			bValidInput = false
			exit function
		end if
		
		call SendActivateEmail(Session("userId"),strFirstName,strLastName,strEmail)
	end if
	
	validateInput = true
end function

private sub DeactivateAccount(userId)
	dim sql
	sql = "UPDATE ubdk_users SET tintActive = 0 WHERE userId = '" & userId & "'"
	boolChangeEmail = true
	oCon.Execute(sql)
	'Response.Write(sql)
end sub

public sub SendActivateEmail(userId,firstName,lastName,email)
	dim mail ' as JMail.SMTPMail
	dim body
	set mail = Server.CreateObject("JMail.SMTPMail")

		body = "Hej " & firstname & vbCrLf &_
				vbCrLf &_
				"Du modtager denne email, da du har skiftet emailadresse p� [usedbooks.dk]" & VbCrLf &_
				VbCrLf &_
				"For at sikre, at din emailadresse er korrekt, har vi deaktiveret din konto. Det eneste du dog skal g�re for at aktivere den, er at klikke p� linket herunder:" &_
				VbCrLf &_
				VbCrLf &_
				"http://" & Application("uriprefix") & "usedbooks.dk/admin/activate?tempId=" & Server.URLEncode(Session("userId")) & "&checksum=" & md5(application("initVector") & Session("userId")) & VbCrLf &_
				VbCrLF &_
				"Skulle der opst� problemer, s� kan du altid kontakte os ved at svare p� denne email." & VbCrLf &_
				VbCrLf &_
				VbCrLf &_
				"Med venlig hilsen" & VbCrLF &_
				"Jesper og Morten" & VbCrLf &_
				VbCrLf &_
				"http://www.usedbooks.dk"

	on error resume next
		With mail
			.Subject = "Re-aktiveringsemail til usedbooks.dk"
			.Body = body
			.Sender = "support@usedbooks.dk"
			.SenderName = "usedbooks.dk"
			.AddRecipient email
			'.AddRecipientBcc "adm@usedbooks.dk"
			'.AddRecipientBcc "morten@usedbooks.dk"
			'.AddRecipientBcc "jesper@usedbooks.dk"
			.ServerAddress = "mail.jirty.dk"
			.AddHeader "Originating-IP",Request.ServerVariables("REMOTE_ADDR")
			.AddHeader "HTTP_VIA",Request.ServerVariables("HTTP_VIA")
			.AddHeader "HTTP_X_FORWARDED_FOR",Request.ServerVariables("HTTP_X_FORWARDED_FOR")
			.Execute
		End With
	'Response.Write Err.number
	if Err.number = 0 then
		strMessage = "Vi har sendt en email til adressen <b>" &  strEmail & "</b>.<br/><br/>Du vil snarest modtage en email fra os. Denne email vil indeholde et link til vores website. N�r du klikker p� dette link, s� vil din profil blive aktiveret. Har du ikke modtaget denne email i l�bet af en time, s� beder vi dig kontakte os via linket i menuen til venstre.<br/><br/>Teamet bag usedbooks.dk."
	elseif Err.number = -2147418113 then
		strMessage = "Det var ikke muligt at sende dig en bekr�ftelsesemail. Dette skyldes sikkert, at den emailadresse du opgav var forkert. Din profil kan derfor ikke aktiveres. Kontakt os venligst ang�ende dette via link i menuen til venstre.<br/><br/>Teamet bag usedbooks.dk."
	end if
	on error goto 0
	DeactivateAccount(Session("userId"))
end sub

if validateInput then
	strSQL = " UPDATE ubdk_users SET " &_
				"strFirstName = '" & strFirstName & "', " &_
				"strLastName = '" & strLastName & "', " &_
				"strEmail = '" & strEmail & "', " &_
				"strPhoneNumber = '" & strPhone & "', "
				
				if Len(lngMobile) <> 0 then
				strSql = strSql & "intMobile = " & lngMobile & ", "
				else
				strSql = strSql & "intmobile = null,"
				end if
				
				if Len(strPasswordNy) > 3 then
				strSql = strSql & "strPasswordMD5 = '" & md5(strPasswordNy) & "', "
				'Response.Write "Nyt password:" & strPasswordNy
				end if
				strSql = strSql & "tintPublicAddress = " & intPublic
				
				SELECT CASE intEmail
					CASE 1
						strSql = strSql & ",tintEmailOperations = 1, tintEmailGeneral = 1"
					CASE 2
						strSql = strSql & ",tintEmailOperations = 1, tintEmailGeneral = 0"
					CASE 3
						strSql = strSql & ",tintEmailOperations = 0, tintEmailGeneral = 1"
					CASE 0
						strSql = strSql & ",tintEmailOperations = 0, tintEmailGeneral = 0"
				END SELECT
			strSql = strSql & " WHERE userId = '" & Session("userId") & "'"
			
	'response.Write strSql
	'response.end			
	oCon.Execute(strSql)
	
    session("strFirstName") = strFirstName
    session("strLastName") = strLastName
    session("strEmail") = strEmail
    
    if boolChangeEmail then
		Response.Redirect("/admin/logout")
	else
		Response.Redirect("../")
	end if
end if


%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>