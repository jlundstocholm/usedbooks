<%@Language="VBScript"%>
<%
option explicit

if not session("logonValid") = true then
   Response.Redirect("/")
end if
   
Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.Expires = 0

%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
dim strSqlProfile
dim oRsProfile ' as ADODB.Recordset

set oRsProfile = Server.CreateObject("ADODB.Recordset")
'strSqlProfile = "SELECT FirstName,LastName,Email,Phone,Mobile,AddressPublic FROM viewUsers WHERE id=" & Session("userId")
strSqlProfile = "SELECT userId, strFirstName, strLastName, strEmail, strPasswordMD5, tintStudy, tintActive, tintRoleId, strPhoneNumber, intMobile, tintPasswordUseOnce, tintPublicAddress, tintEmailOperations, tintEmailGeneral FROM ubdk_users WHERE userId = '" & session("userId") & "'"

with oRsProfile
	.ActiveConnection =	oCon
	.CursorLocation =	3 ' adUseclient
	.CursorType =		0 ' adOpenForwardOnly
	.LockType =			1 ' adLockReadOnly
	.Source =			strSqlProfile
	.Open
end with

%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>