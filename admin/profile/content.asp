<!-- #include virtual="/include/navlinks_user.asp" -->
<h1>Brugerprofil</h1>
<div class="content-indent">Herunder er dine brugeroplysninger. For at kunne rette i dem er det n�dvendigt at indtaste dit password. �nsker du at lave et nyt password, s� kan dette g�res nederst. Husk at felter med asterisk (<span class="asterisk-smoke">&#42;</span>) er obligatoriske.</div><br/>

<form method="post" action="change/default.asp" id="formEdit" onsubmit="return validateEditProfile(document.forms.formEdit.fornavn.value,document.forms.formEdit.efternavn.value,document.forms.formEdit.email.value,document.forms.formEdit.mobil.value,document.forms.formEdit.nytpassword.value,document.forms.formEdit.nytpassword2.value);">
<table border=0 class="formular">
    <tr>
		<td class="cell-center-vertical">&nbsp;Fornavn:</td>
		<td>&nbsp;<span class="asterisk">*</span></td>
		<td><input name = "fornavn" value="<% = Server.HTMLEncode(oRsProfile("strFirstName")) %>" type = "text" size = "50" maxlength = "255"/></td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Efternavn:</td>
		<td>&nbsp;<span class="asterisk">*</span></td>
		<td><input name = "efternavn" value="<% = Server.HTMLEncode(oRsProfile("strLastName")) %>" type = "text" size = "50" maxlength = "255"/></td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Email adresse:</td>
		<td>&nbsp;<span class="asterisk">*</span></td>
		<td><input name = "email" value="<% = Server.HTMLEncode(oRsProfile("strEmail")) %>" type = "text" size = "40" maxlength="255" id="Text2"/><br/>
		<span style="font-style: italic;">Hvis du skifter emailadresse, vil du blive logget ud af systemet og din konto vil blive deaktiveret. Du vil herefter modtage en email med et aktiveringslink sendt til den nye emailadresse, der �bner din konto igen.</span></td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Telefonnummer:</td>
		<td>&nbsp;</td>
		<td><input name = "telefon" value="<% if Len(oRsprofile("strPhoneNumber")) > 0 then Response.Write Server.HTMLEncode(oRsProfile("strPhoneNumber")) end if %>" type = "text" size = "8" maxlength = "15" /></td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Mobilnummer:</td>
		<td>&nbsp;</td>
		<td><input name = "mobil" value="<% if not isNull(oRsProfile("intMobile")) then Response.Write Server.HTMLEncode(oRsProfile("intMobile")) end if %>" type = "text" size = "8" maxlength = "15" id="Text1"/></td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Kontaktdata:</td>
		<td>&nbsp;</td>
		<td><input <% if CInt(oRsProfile("tintPublicAddress")) = 1 then response.Write "checked=""checked""" end if %> type="radio" name="offentlig" value="1" id="Radio1"/>Offentlige&nbsp;&nbsp;
			<input <% if CInt(oRsProfile("tintPublicAddress")) = 0 then response.Write "checked=""checked""" end if %> type="radio" name="offentlig" value="0" id="Radio2"/>&nbsp;Private</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td>
			<i>
				Hvis du v�lger &quot;Private&quot;, s� vil kun dit navn blive vist. Det vil
				i stedet v�re muligt at kontakte dig direkte via vores website. Dine
				oplysninger vil i dette tilf�lde ikke v�re tilg�ngelige for bes�gende og
				andre registrerede brugere.</i>
		</td>
    </tr>
	<tr>
		<td class="cell-center-vertical">&nbsp;Nyhedsbreve</td>
		<td>&nbsp;</td>
		<td>
			<input
			<%
				if oRsProfile("tintEmailOperations") = 1 AND oRsProfile("tintEmailGeneral") = 1 then
					Response.Write "checked=""checked"""
				end if
			%>
			type="radio" name="letters" value="1" id="Radio3"/>Alle&nbsp;&nbsp;
			<input
			<%
				if oRsProfile("tintEmailOperations") = 1 AND oRsProfile("tintEmailGeneral") = 0 then
					Response.Write "checked=""checked"""
				end if
			%>
			 type="radio" name="letters" value="2" id="Radio4"/>Kun driftemails&nbsp;&nbsp;
			<input
			<%
				if oRsProfile("tintEmailOperations") = 0 AND oRsProfile("tintEmailGeneral") = 1 then
					Response.Write "checked=""checked"""
				end if
			%>
			 type="radio" name="letters" value="3" id="Radio5"/>Kun generelle emails&nbsp;&nbsp;
			<input
			<%
				if oRsProfile("tintEmailOperations") = 0 AND oRsProfile("tintEmailGeneral") = 0 then
					Response.Write "checked=""checked"""
				end if
			%>
			 type="radio" name="letters" value="0" id="Radio6"/>Ingen emails
		</td>
    </tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td>
			<i>
				Fra tid til anden har vi behov for at kunne skrive kontakte vores
				brugere via email. Disse emails vil v�re enten emails om driften
				af vores website eller generelle emails om nye tiltag, features etc.
				</i>
		</td>
    </tr>
    <!--
    <tr>
	<td>&nbsp;Password:</td>
	<td>&nbsp;</td>
	<td><input name = "password" type = "password" size = "15" maxlength = "15" />&nbsp;Dit password skal altid angives, n�r du retter i din profil.</td>
    </tr>
    -->
    <tr>
	<td class="cell-center-vertical">&nbsp;Nyt Password:</td>
	<td>&nbsp;</td>
	<td><input name = "nytpassword" type = "password" size = "15" maxlength = "15" /></td>
    </tr>
    <tr>
	<td class="cell-center-vertical">&nbsp;Bekr�ft:</td>
	<td>&nbsp;</td>
	<td><input name = "nytpassword2" type = "password" size = "15" maxlength = "15" /></td>
    </tr>
    <tr>
		<td colspan="3"><input type = "submit" value = "OK" />&nbsp;<input type = "reset" value = "Slet" /></td>
    </tr>
</table>

</form>
