<!-- #include virtual="/include/navlinks_user.asp" -->
<h1>Direkte kontakt</h1>
<div class="content-indent">
	Er du blevet kontaktet om dine b�ger via &quot;Direkte Kontakt&quot;, s�
	kan du se et arkiv over dem her.
</div>
<div class="content-indent">
	<table>
		<tr>
			<td colspan="2"><hr/></td>
		</tr>
		<% dim j	' as integer
		   j = 0
		   do while not oRsContact.EOF 
		   j = j + 1
		%>
		<tr style="font-weight: bold;">
			<td style="text-align:right;"><% = j %>.</td><td>Fra:&nbsp;<% = oRsContact("strName") %><% if Len(oRsContact("strEmail")) <> 0 then %>&nbsp;(<a href="mailto:<% = oRsContact("strEmail") %>"><% = oRsContact("strEmail") %></a>)<% end if %></td> 
		</tr>
		<% if not Len(oRsContact("strPhone")) = 0 then %>
		<tr>
			<td>&nbsp;</td><td>Telefonnummer: <% = oRsContact("strPhone") %></td>
		</tr>
		<% end if %>
		<% if not Len(oRsContact("strMobile")) = 0 then %>
		<tr>
			<td>&nbsp;</td><td>Mobil: <% = oRsContact("strMobile") %></td>
		</tr>
		<% end if %>
		<tr>
			<td>&nbsp;</td><td>Ang�ende bogen: <% = oRsContact("strBookTitle") %></td>
		</tr>
		<tr>
			<td>&nbsp;</td><td>Tidspunkt: <% = oRsContact("dtNow") %></td>
		</tr>
		<tr>
			<td>&nbsp;</td><td>Besked:</td>
		</tr>
		<tr>
			<td>&nbsp;</td><td><% = Replace(Server.HTMLEncode(oRsContact("strMsg")),vbCrLf,"<br/>") %></td>
		</tr>
		<tr>
			<td colspan="2"><hr/></td>
		</tr>
		<% oRsContact.MoveNext()
		   loop
		%>
	</table>
</div>
