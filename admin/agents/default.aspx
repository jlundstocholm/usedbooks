<%@ Page 
	language="c#" 
	src="default.aspx.cs" 
	AutoEventWireup="false" 
	Inherits="usedbooks.admin.agents" 	
%>

<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
		<title>[usedbooks.dk] - k&oslash;b og salg af brugte b&oslash;ger - for studerende ved DTU.</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0" />
		<link href="/styles/default.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="/scripts/enhanceSearchBar.js" >
		</script>
		<script type="text/javascript" src="/scripts/validateInput.js" >
		</script>
	</head>
	<body >
		<table class="headertable" width="760" id="Table1">
			<tr>
				<td class="logoelement">
					<a href="/" title="Tilbage til forsiden"><img src="/images/usedbooks.dk.jpg" alt="usedbooks.dk" height="84" width="337" /></a>
				</td>
				<td align="right">
					<form method="get" action="/search" id="Form1">
						<div>
							<input  onfocus="deleteTextFromField()" size="30" class="white" value="" type="text" name="query" id="Text1"/><input  type="submit" value="S�g" /><br />
							<span>ISBN</span><input   type="radio" name="search" value="isbn" id="Radio1"/>&nbsp;
							<span>Titel</span><input   type="radio" name="search" value="title" id="Radio2"/>&nbsp;
							<span>Forfatter</span><input   type="radio" name="search" value="author" id="Radio3"/>&nbsp; 
							Alle felter<input   type="radio" name="search" value="all" id="Radio4"/><br/>
							<select  onchange="stripData()" class="gray" id="Select1" name="courses">
								<option value="">V�lg kursus (listen indeholder kun kurser med b�ger)</option>
								
							</select>
						</div>
					</form>
				</td>
			</tr>
		</table>
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					&lt;menu mangler &gt;
				</td>
				<td class="leftline">
					Det bliver i n�r fremtid muligt at definere agenter, s� man
					automatisk modtager en email i det �jeblik en bruger s�tter
					en bog til salg, som matcher dit kriterium.<br/>
					<br/>
					[usedbooks.dk]
				</td>
			</tr>

		</table>
  </body>
</html>
