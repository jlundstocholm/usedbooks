<% if not bValidInput then %>
<h1>Der skete en fejl under oprettelsen</h1>
<div class="content-indent"><% = strMessage %></div>
<br/>
<div class="content-indent">G� tilbage og ret fejlen eller kontakt os (brug "Kontakt os" i venstre menu).<br/><br/>[usedbooks.dk]</div>
<% else %>
<h1>Du er blevet oprettet i vores system</h1>
<div class="content-indent">
	<% = strMessage %>
</div>
<% end if %>