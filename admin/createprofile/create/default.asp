<%@Language="VBScript"%>
<%
option explicit

if Session("logonValid") then
	Response.Redirect("/")
end if

%>
<!-- #include virtual="/utilities/md5.asp" -->
<%

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.CacheControl = "no-cache"
Response.AddHeader "pragma","no-cache"
%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
dim strMessage
dim strFirstName,strLastName,strEmail,strPhone,strPassword,strPasswordConf,strGuid ' as string
dim lngMobile ' as long
dim intPublic,intEmail ' as integer
dim bValidInput ' as boolean

bValidInput = true

public function validateInput
	validateInput = false
	
	strFirstName = Replace(Request.Form("fornavn"),"'","''")
	strLastName = Replace(Request.Form("efternavn"),"'","''")
	strEmail = Replace(Request.Form("email"),"'","''")
	strPhone = Replace(Request.Form("telefon"),"'","''")
	lngMobile = Replace(Request.Form("mobil")," ","")
	strPassword = Replace(Request.Form("password"),"'","''")
	strPasswordConf = Replace(Request.Form("password2"),"'","''")
	intPublic = Request.Form("offentlig")
	intEmail = Request.Form("letters")
	
	if Len(strFirstName) = 0 then
		strMessage = "Du skal angive dit fornavn for at oprette en profil."
		bValidInput = false
		exit function
	end if
	
	if Len(strLastName) = 0 then
		strMessage = "Du skal angive dit efternavn for at oprette en profil."
		bValidInput = false
		exit function
	end if
	
	if Len(strEmail) = 0 then
		strMessage = "Du skal angive en emailadresse for at oprette en profil. " &_
					"V�r opm�rksom p�, at vi bruger emailadressen til at sende en " &_
					"bekr�ftelsesemail til dig, s� det er vigtigt, at den er korrekt. " &_
					"Hvis du ikke kan modtage emailen, s� kan du ikke aktivere din profil."
		bValidInput = false
		exit function
	end if
	
	if not (Len(lngMobile) = 0 OR Len(lngMobile) = 8) then
		strMessage = "Dit mobilnummer skal best� af 8 cifre eller ikke v�re angivet."
		bValidInput = false
		exit function
	end if
	
	if Len(lngMobile) <> 0 then
		if not (isNumeric(lngMobile) AND Len(lngMobile) = 8) then
			strMessage = "Dit mobilnummer skal best� af 8 cifre, dvs ingen bogstaver, " &_
						"bindestreger, punktummer eller lignende."
			bValidInput = false
			exit function
		end if
	end if
	
	if Len(strPassword) < 4 then
		strMessage = "Dit password skal v�re mindst 4 tegn langt."
		bValidInput = false
		exit function
	end if
	
	if strPassword <> strPasswordConf then
		strMessage = "Dit password samt bekr�ftelsen af det skal v�re ens."
		bValidInput = false
		exit function
	end if
	
	if not isNumeric(intPublic) then
		strMessage = "Du kan kun v�lge imellem at have dine oplysninger offentlige " &_
					"eller undlade det."
		bValidInput = false
		exit function
	end if
	
	if not (intPublic = 1 OR intPublic = 0) then
		strMessage = "Du kan kun v�lge imellem at have dine oplysninger offentlige " &_
					"eller undlade det."
		bValidInput = false
		exit function
	end if
	
	if not isNumeric(intEmail) then
		strMessage = "Du skal tage stilling til hvilke nyhedsbreve du vil modtage."
		bValidInput = false
		exit function
	end if
	
	if not (intEmail >= 0 AND intEmail < 4) then
		strMessage = "Du skal tage stilling til hvilke nyhedsbreve du vil modtage."
		bValidInput = false
		exit function
	end if
	
	dim oRsCheckEmail ' as ADODB.Recordset
	set oRsCheckEmail = Server.CreateObject("ADODB.Recordset")
	dim strSQLCheck ' as string
	strSqlCheck = "SELECT COUNT(strEmail) As CountOfUsers FROM ubdk_users WHERE strEmail = '" & strEmail & "'"
	with orsCheckEmail
		.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseclient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =			1 ' adLockReadOnly
		.Source =			strSqlCheck
		.Open
	end with
	
	if CInt(oRsCheckEmail("CountOfUsers")) <> 0 then
		strMessage = "Din angivne emailadresse findes allerede i databasen. " & _
					"Hvis du er sikker p�, at emailadressen er din, s� kan du f� " &_
					"tilsendt et password til profilen ved at bruge " &_
					"linket ""Glemt password?"" i menuen til venstre."
		oRsCheckEmail.Close
		set oRsCheckEmail = nothing
		bValidInput = false
		exit function
	end if
	
	validateInput = true
end function

if validateInput then
	call insertData
	'response.Write "nu s�tter vi data ind"
end if

private function insertData
	dim strSqlMake
	
	strSqlMake = "INSERT INTO ubdk_users (userId,strFirstName,strLastName,strEmail,strPasswordMD5"
	
	if Len(strPhone) <> 0 then
		strSqlMake = strSqlMake & ",strPhoneNumber"
	end if
	
	if Len(lngMobile) <> 0 then
		strSqlMake = strSqlMake & ",intMobile"
	end if
	
	
	dim oGuid ' as Scriptlet.Typelib
	set oGuid = Server.CreateObject("Scriptlet.TypeLib")
	strGuid = Left(oGuid.GUID,38)
	strSqlMake = strSqlMake & ",tintStudy,tintRoleId,tintActive,dtCreateProfile,tintEmailOperations,tintEmailGeneral) VALUES ('" & strGuid & "','" & strFirstName & "','" & strLastName & "','" & strEmail & "','" & md5(strPassword) & "'"
	
	if Len(strPhone) <> 0 then
		strSqlMake = strSqlMake & ",'" & strPhone & "'"
	end if
	
	if Len(lngMobile) <> 0 then
		strSqlMake = strSqlMake & "," & lngMobile
	end if
	strSqlMake = strSqlMake & ",1,2,0,Now()"
	
	SELECT CASE intEmail
		CASE 1
			strSqlMake = strSqlMake & ",1,1)"
		CASE 2
			strSqlMake = strSqlMake & ",1,0)"
		CASE 3
			strSqlMake = strSqlMake & ",0,1)"
		CASE 0
			strSqlMake = strSqlMake & ",0,0)"
	END SELECT

	oCon.Execute(strSqlMake)
	
	call sendEmail
end function

private function sendEmail
	dim mail ' as JMail.SMTPMail
	dim strSQLId,body,fornavn ' as string
	dim oRsId ' as ADODB.Recordset
	
	set oRsId = Server.CreateObject("ADODB.Recordset")
	strSqlId = "SELECT userId FROM ubdk_users WHERE strEmail = '" & strEmail & "'"
	with oRsId
		.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseclient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =			1 ' adLockReadOnly
		.Source =			strSqlId
		.Open
	end with
	
		'set mail = Server.CreateObject("JMail.SMTPMail")
		set mail = Server.CreateObject("JMail.SMTPMail")
		
		fornavn = strFirstName

		body = "Hej " & fornavn & vbCrLf &_
				vbCrLf &_
				"Tak fordi du har registreret dig p� [usedbooks.dk]." & VbCrLf &_
				VbCrLf &_
				"Klik p� aktiveringslinket herunder for at aktivere din profil:" &_
				VbCrLf &_
				VbCrLf &_
				"http://" & Application("uriprefix") & "usedbooks.dk/admin/activate?tempId=" & Server.URLEncode(oRsId("userId")) & "&checksum=" & md5(application("initVector") & oRsId("userId")) & VbCrLf &_
				VbCrLF &_
				"Skulle der opst� problemer, s� kan du altid kontakte os ved at svare p� denne email." & VbCrLf &_
				VbCrLf &_
				VbCrLf &_
				"Med venlig hilsen" & VbCrLF &_
				"Jesper og Morten" & VbCrLf &_
				VbCrLf &_
				"http://www.usedbooks.dk"
				
	on error resume next
		With mail
			.Subject = "Aktiveringsemail til usedbooks.dk"
			.Body = body
			.Sender = "support@usedbooks.dk"
			.SenderName = "usedbooks.dk"
			.AddRecipient strEmail
			'.AddRecipientBcc "adm@usedbooks.dk"
			'.AddRecipientBcc "morten@usedbooks.dk"
			'.AddRecipientBcc "jesper@usedbooks.dk"
			.ServerAddress = Application("smtpserver")
			'.AddHeader "Originating-IP",Request.ServerVariables("REMOTE_ADDR")
			'.AddHeader "HTTP_VIA",Request.ServerVariables("HTTP_VIA")
			'.AddHeader "HTTP_X_FORWARDED_FOR",Request.ServerVariables("HTTP_X_FORWARDED_FOR")
			.Execute
			'.nq
		End With
	if Err.number = 0 then
		strMessage = "Vi har sendt en email til adressen <b>" &  strEmail & "</b>.<br/><br/>Du vil snarest modtage en email fra os. Denne email vil indeholde et link til vores website. N�r du klikker p� dette link, s� vil din profil blive aktiveret. Har du ikke modtaget denne email i l�bet af en time, s� beder vi dig kontakte os via linket i menuen til venstre.<br/><br/>Teamet bag usedbooks.dk."
	else 'if Err.number = -2147418113 then
		strMessage = "Det var ikke muligt at sende dig en bekr�ftelsesemail. Dette skyldes sikkert, at den emailadresse du opgav var forkert. Din profil er blevet oprettet, men den er ikke blevet aktiveret. Derfor kan du ikke bruge den endnu. Kontakt os venligst ang�ende dette via link i menuen til venstre.<br/><br/>Teamet bag usedbooks.dk."
	end if
	on error goto 0
end function

%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>