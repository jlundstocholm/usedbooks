<%
	'Response.Write "Det er desv�rre ikke muligt at oprette en profil lige nu. Dette skyldes vedligeholdelse af sitet.<br/><br/>Vi regner med at v�re online igen mandag morgen.<br/><br/>Teamet bag usedbooks.dk ."
	'Response.End
%>
<h1>Ny bruger</h1>
<div class="content-indent">
	Du skal udfylde felterne nedenfor for at blive oprettet som bruger p� 
	usedbooks.dk og dermed kunne s�tte b�ger til salg via dette site.<br/>
	<br/>
	Felter markeret med asterisk (<span class="asterisk-smoke">&#42;</span>) er obligatoriske. Din brugerprofil 
	oprettes n�r du klikker p� "Opret bruger". Herved accepterer du de <a href="/useragreement/">vilk�r</a> der er for brugen af usedbooks.dk.
</div>
<br/>

<form method="post" action="create/default.asp" id="createUser" onsubmit="return checkNewUser(document.forms.createUser.fornavn.value,document.forms.createUser.efternavn.value,document.forms.createUser.email.value,document.forms.createUser.telefon.value,document.forms.createUser.mobil.value,document.forms.createUser.password.value,document.forms.createUser.password2.value,document.forms.createUser.offentlig[0].checked,document.forms.createUser.offentlig[1].checked);">
<table class="formular">
    <tr>
		<td class="cell-center-vertical">&nbsp;Fornavn:</td><td>&nbsp;<span class="asterisk">*</span></td>
		<td><input name="fornavn" type="text" size="50" maxlength="30"/>&nbsp;</td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Efternavn:</td><td>&nbsp;<span class="asterisk">*</span></td>
		<td><input name="efternavn" type="text" size="50" maxlength="30"/></td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Emailadresse:</td><td>&nbsp;<span class="asterisk">*</span></td>
		<td><input name="email" type="text" size="50" /></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td>
			<i>
				Det er vigtigt, at den emailadresse du angiver er korrekt, da vi sender en
				bekr�ftelsesemail til dig efter oprettelsen. Hvis emailadressen er forkert,
				s� n�r emailen ikke frem, og du vil ikke kunne logge ind p� din konto.
			</i>
		</td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Telefonnummer:</td><td>&nbsp;</td>
		<td><input name="telefon" type="text" size="8" maxlength="8" /></td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Mobil:</td><td>&nbsp;</td>
		<td><input name="mobil" type="text" size="8" maxlength="8" /></td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Password:</td><td>&nbsp;<span class="asterisk">*</span></td>
		<td>
			<input name="password" type="password" size="15" maxlength="15" /><td/>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td>
			<i>Dit password skal v�re p� mindst 4 tegn.</i>
		</td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Bekr�ft&nbsp;password:</td><td>&nbsp;<span class="asterisk">*</span></td>
		<td><input name="password2" type="password" size="15" maxlength="15" onPaste="return false"/></td>
    </tr>
    <tr>
		<td >&nbsp;Kontaktoplysninger:</td><td>&nbsp;</td>
		<td>
			<input checked="checked" type="radio" name="offentlig" value="1"/>Offentlig&nbsp;&nbsp;
			<input type="radio" name="offentlig" value="0"/>&nbsp;Privat</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td>
			<i>
				V�lger du &quot;offentlig&quot; vil din emailadresse og dine telefonnumre blive vist i kontakt-feltet ved hver af dine b�ger. Hvis du v�lger &quot;privat&quot;, s� vil kun dit navn blive vist.</i>
		</td>
    </tr>
	<tr>
		<td>&nbsp;Nyhedsbreve</td><td>&nbsp;&nbsp;</td>
		<td>
			<input checked="checked" type="radio" name="letters" value="1"/>Alle&nbsp;&nbsp;
			<input type="radio" name="letters" value="2" />Kun driftemails&nbsp;&nbsp;
			<input type="radio" name="letters" value="3" />Kun generelle emails&nbsp;&nbsp;
			<input type="radio" name="letters" value="0" />Ingen emails
		</td>
    </tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td>
			<i>
				Fra tid til anden har vi behov for at kunne skrive kontakte vores
				brugere via email. Disse emails vil v�re enten emails om driften
				af vores website eller generelle emails om nye tiltag, features etc.
				</i>
		</td>
    </tr>
    <tr>
		<td colspan="2">&nbsp;</td>
		<td><input type="submit" value="Opret bruger"/>&nbsp;<input type="reset" value="Slet"/></td>
    </tr>
</table>
</form>
	