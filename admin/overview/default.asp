<%@Language="VBScript"%>
<%
option explicit

if Application("offline") then
	Response.Redirect("/operations")
end if

if not Session("logonValid") then
	Response.Redirect("/")
end if
Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.Expires = 0
%>
<!-- #include virtual="/utilities/md5.asp" -->
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
	dim strSqlLogin ' as string
	dim dtLastLogin ' as DateTime
	strSqlLogin = "SELECT dtLogin FROM ubdk_accesslog WHERE userId = '" & Session("userid") & "' ORDER BY dtLogin DESC  LIMIT 0,2"
	
	dim oRsLogin
	set orsLogin = Server.CreateObject("ADODB.Recordset")
	with oRsLogin
		.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseClient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =			1 ' adReadOnly
		.Source =			strSqlLogin
		.Open
	end with
	do while not oRsLogin.EOF
		dtLastLogin = oRsLogin("dtLogin")
	oRsLogin.MoveNext
	loop
	
%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>