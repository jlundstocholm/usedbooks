<!-- #include virtual="/admin/tools/toolbar.asp" -->
<h1>Administrator-v�rkt�jer</h1>
<div class="content-indent">
</div>
<br/>
<br/>
<div class="content-indent">
	<table width="100%">
		<tr>
			<td>
				<table>
					<tr>
						<td colspan="2"><h2>B�ger og brugere</h2></td>
					</tr>
					<tr>
						<td>Aktive brugere</td><td style="text-align: right;"><% = intActiveUsers %></td>
					</tr>
					<tr>
						<td>Inaktive brugere</td><td style="text-align: right;"><% = intInactiveUsers %></td>
					</tr>
					<tr style="font-weight: bold;">
						<td>Antal brugere i alt</td><td style="text-align: right;"><% = CInt(intActiveUsers) + CInt(intInactiveUsers) %></td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td >Aktive b�ger</td><td style="text-align: right;"><% = intActiveBooks %></td>
					</tr>
					<tr>
						<td >Inaktive b�ger</td><td style="text-align: right;"><% = intInActiveBooks %></td>
					</tr>
					<tr>
						<td >Slettede b�ger</td><td style="text-align: right;"><% = intDeletedBooks %></td>
					</tr>
					<tr>
						<td >Slettede b�ger (af admin)</td><td style="text-align: right;"><% = intDeletedBooksByAdmin %></td>
					</tr>
					<tr style="font-weight: bold;">
						<td>Antal b�ger i alt</td><td style="text-align: right;"><% = CInt(intActiveBooks) + CInt(intInActiveBooks) + CInt(intDeletedBooks) + CInt(intDeletedBooksByAdmin)%></td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td>Samlet sum for inaktive/solgte b�ger</td><td style="text-align: right;font-weight: bold;">Kr. <% = FormatNumber(intSum,2) %></td>
					</tr>
				</table>
			</td>
			<td>
				<table>
					<tr>
						<td colspan="2"><h2>S�gninger</h2></td>
					</tr>
					<tr style="font-weight: bold;">
						<td>Dato</td><td style="text-align: right;">Antal</td>
					</tr>
					<% do while not oRsSearch.EOF %>
					<tr>
						<td><% = oRsSearch("dtDate") %></td><td style="text-align: right;"><% = oRsSearch("NumberOfSearches") %></td>
					</tr>
					<% oRsSearch.MoveNext
					   loop
					%>
				</table>
			</td>
		</tr>
	</table>
	
</div>