<%@ Page language="c#" src="visits.aspx.cs" AutoEventWireup="false" Inherits="usedbooks.admin.tools.stats.visits" %>

<html>
	<head>
		<title>Stats - usedbooks.dk</title>
	</head>
	<body>
		<table>
			<tr>
				<td valign="top">
					<h1>Bes�gende</h1>
					<p>
						<img src="/images/dtu.png" alt="" height="10" width="20"/>&nbsp;DTU<br/>
						<img src="/images/cbs.png" alt="" height="10" width="20"/>&nbsp;CBS
					</p>
					<asp:Repeater id="rVisits" runat="server">
						<HeaderTemplate>
							<table>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td><%# DataBinder.Eval(Container.DataItem, "Day") %></td><td style="font-style:italic;"><img src="/images/<%# DataBinder.Eval(Container.DataItem, "school").ToString().Trim() %>.png" alt="" height="10" width="<%# DataBinder.Eval(Container.DataItem, "Visits").ToString().Trim() %>"/>&nbsp;<%# DataBinder.Eval(Container.DataItem, "Visits").ToString().Trim() %></td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							</table>
						</FooterTemplate>
					</asp:Repeater>
				</td>
				<td valign="top">
					<h1>Solgte b�ger</h1>
					<p>
						<img src="/images/dtu.png" alt="" height="10" width="20"/>&nbsp;DTU<br/>
						<img src="/images/cbs.png" alt="" height="10" width="20"/>&nbsp;CBS
					</p>
					<asp:Repeater id="rMoney" runat="server" OnItemDataBound="CalcWidth">
						<HeaderTemplate>
							<table>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td><%# DataBinder.Eval(Container.DataItem, "Day") %></td><td style="font-style:italic;"><img src="/images/<%# DataBinder.Eval(Container.DataItem, "school").ToString().Trim() %>.png" alt="" height="10" width="<asp:Literal id="liWidth" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Sum") %>'/>"/>&nbsp;kr. <%# DataBinder.Eval(Container.DataItem, "Sum").ToString().Trim() %></td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							</table>
						</FooterTemplate>
					</asp:Repeater>
				</td>
			</tr>
		</table>
	</body>
</html>
