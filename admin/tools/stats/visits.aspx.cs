using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using usedbooks;

using Microsoft.Data.Odbc;

namespace usedbooks.admin.tools.stats
{
	/// <summary>
	/// Summary description for visits.
	/// </summary>
	public class visits : System.Web.UI.Page
	{

		protected System.Web.UI.WebControls.Repeater rVisits;
		protected System.Web.UI.WebControls.Repeater rMoney;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			string strSql = "SELECT c.strSchoolCode As School,count(s.schoolId) As Visits,Left(s.accessTime,10) As Day FROM ubdk_stats s inner join ubdk_school c on s.schoolId = c.id GROUP BY Left(s.accessTime,10), c.strSchool ORDER BY Left(s.accessTime,10) DESC LIMIT 10";
			dbmySql oDb = new dbmySql();
			OdbcDataReader oData = oDb.getData(strSql);
			rVisits.DataSource = oData;
			rVisits.DataBind();

			strSql = "SELECT Left(b.dtDeactivate,10) As 'Day',sum(b.intPrice) As 'Sum',s.strSchoolCode As 'school' FROM ubdk_books b inner join ubdk_users u on b.userId = u.userId inner join ubdk_school s on u.tintStudy = s.id WHERE b.dtDeactivate not in ('0000-00-00 00:00:00') GROUP BY Left(b.dtDeactivate,10) , s.strSchoolCode ORDER BY Left(b.dtDeactivate,10) DESC LIMIT 10";
			oData = oDb.getData(strSql);
			rMoney.DataSource = oData;
			rMoney.DataBind();

		}

		protected void CalcWidth(object sender, RepeaterItemEventArgs e) 
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) 
			{
//				int s = e.Item.[1];
//				DataRowView oRowValue = (DataRowView)e.Item.DataItem;
				int a = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem,"Sum"));
				double s = Math.Floor(a/30);
//				int iWidth= Convert.ToInt32(oRowValue["Sum"]);
				Literal oLiteral = (Literal)e.Item.FindControl("liWidth");
				oLiteral.Text = s.ToString();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
