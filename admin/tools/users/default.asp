<%@Language="VBScript"%>
<%
option explicit

if not (session("logonValid") = true  AND Session("tintRole") = 1 )then
   Response.Redirect("/")
end if
%>
<!-- #include virtual="/utilities/md5.asp" -->
<%

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.CacheControl = "no-cache"
Response.AddHeader "pragma","no-cache"

%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
dim oRsUsers 
set oRsUsers = Server.CreateObject("ADODB.Recordset")
strSQL = "SELECT u.tintActive,u.userId,u.strFirstName,u.strLastName,u.strEmail,r.strDescription, COUNT(b.userid) As CountOfBooks FROM (ubdk_users u INNER JOIN ubdk_roles r ON u.tintRoleId = r.tintUserLevel) LEFT JOIN ubdk_books b ON b.userid = u.userID GROUP BY u.strFirstName,u.strLastName,u.strEmail,r.strDescription,u.userID ORDER BY u.tintActive DESC,u.strFirstName ASC"
with oRsUsers
	.ActiveConnection =	oCon
	.CursorLocation =	3 ' adUseClient
	.CursorType =		0 ' adOpenForwardOnly
	.LockType =			1 ' adReadOnly
	.Source =			strSQL
	.Open
end with

%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>

<%
oCon.Close
set oCon = nothing
%>