<%@Language="VBScript"%>
<%
' /admin/todo/default.asp
'option explicit

if not (session("logonValid") = true  AND Session("tintRole") = 1 )then
   Response.Redirect("/")
end if
%>
<!-- #include virtual="/utilities/md5.asp" -->
<%

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.CacheControl = "no-cache"
Response.AddHeader "pragma","no-cache"

%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
dim oRsTodo 
set oRsTodo = Server.CreateObject("ADODB.Recordset")
strSQL = "SELECT * FROM ubdk_todo WHERE intActive = 1 ORDER BY dtCreateDate ASC"
with oRsTodo
	.ActiveConnection =	oCon
	.CursorLocation =	3 ' adUseClient
	.CursorType =		0 ' adOpenForwardOnly
	.LockType =			1 ' adReadOnly
	.Source =			strSQL
	.Open
end with

%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>

<%
oCon.Close
set oCon = nothing
%>