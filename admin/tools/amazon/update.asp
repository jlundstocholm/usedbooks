<%@Language="VBScript"%>
<%
option explicit

if not (session("logonValid") = true  AND Session("tintRole") = 1 )then
   Response.Redirect("/")
end if

dim strid,strupd

strid =  "'" & Replace(request.Form("haspicture")  ,", ","','") & "'"
strupd = "'" & Replace(request.Form("hasbnchecked"),", ","','") & "'"

dim strsql
dim oCon
	set oCon = Server.CreateObject("adodb.connection")
	oCon.Open application("dsn")

if len(strid) > 2 then
	strsql = "update ubdk_books set tinthasimageonamazon=1 where bookid in (" & strid & ")"
	
	oCon.Execute(strsql)
end if

if len(strupd) > 2 then
  strsql = "update ubdk_books set tintAmazonChecked = 1 where bookid IN (" & strupd & ")"
  oCon.Execute(strsql)
  
end if
Response.Redirect "default.asp"
%>