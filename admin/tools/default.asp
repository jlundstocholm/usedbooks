<%@Language="VBScript"%>
<%
option explicit

if not session("tintRole") = 1 then
   Response.Redirect("/")
end if

Response.Cookies("admin") = 1
   
Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.Expires = 0

%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%

dim strSqlStats,strSqlSearch ' as string
dim oRsStats,oRsSearch ' as ADODB.Recordset
dim intActiveUsers,intInactiveUsers,intActiveBooks,intInactiveBooks,intSum, intDeletedBooks,intDeletedBooksByAdmin ' as integer
'strSqlStats = "SELECT AntalBoeger,ActiveBooks,InactiveBooks,AntalBrugere,ActiveUsers,InactiveUsers,AntalBesoeg FROM getStatValues"
strSqlSearch = "SELECT Left(dtDate,10) As dtDate, COUNT(Left(dtDate,10)) As NumberOfSearches FROM ubdk_search GROUP BY Left(dtDate,10) ORDER BY 1 DESC LIMIT 0,10"

set oRsSearch = Server.CreateObject("ADODB.Recordset")

with oRsSearch
	.ActiveConnection =	oCon
	.CursorLocation =	3 ' adUseclient
	.CursorType =		0 ' adOpenForwardOnly
	.LockType =			1 ' adLockReadOnly
	.Source =			strSqlSearch
	.Open
end with


' number of active users
strSqlStats = "SELECT COUNT(*) As ActiveUsers FROM ubdk_users WHERE tintActive = 1"
oRsStats = oCon.Execute(strSqlStats)
intActiveUsers = oRsStats("ActiveUsers")

' number of inactive users
strSqlStats = "SELECT COUNT(*) As InActiveUsers FROM ubdk_users WHERE tintActive = 0"
oRsStats = oCon.Execute(strSqlStats)
intInActiveUsers = oRsStats("InActiveUsers")

' number of active books
strSqlStats = "SELECT COUNT(*) As ActiveBooks FROM ubdk_books WHERE tintActive = 1"
oRsStats = oCon.Execute(strSqlStats)
intActiveBooks = oRsStats("ActiveBooks")

' number of inactive books
strSqlStats = "SELECT COUNT(*) As InActiveBooks FROM ubdk_books WHERE tintActive = 0"
oRsStats = oCon.Execute(strSqlStats)
intInActiveBooks = oRsStats("InActiveBooks")

strSqlStats = "SELECT COUNT(*) As DeletedBooks FROM ubdk_books WHERE tintActive = -1"
oRsStats = oCon.Execute(strSqlStats)
intDeletedBooks = oRsStats("DeletedBooks")

' number of books deleted by admin
strSqlStats = "SELECT COUNT(*) As DeletedBooksByAdmin FROM ubdk_books WHERE tintActive = -2"
oRsStats = oCon.Execute(strSqlStats)
intDeletedBooksByAdmin = oRsStats("DeletedBooksByAdmin")

' sum of price of inactive books
strSqlStats = "SELECT SUM(intPrice) As SumOfBooks FROM ubdk_books WHERE tintActive < 1 AND tintActive > -2"
oRsStats = oCon.Execute(strSqlStats)
intSum = oRsStats("SumOfBooks")


%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>