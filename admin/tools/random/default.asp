<%@ Language="VBScript" %>
<%

Response.ContentType = "text/plain"


set oCon = Server.CreateObject("ADODB.Connection")
set oRs = Server.CreateObject("ADODB.Recordset")

oCon.Open Application("dsn")

strSql = "SELECT strBookTitle FROM ubdk_books WHERE tintActive = 1 ORDER BY Rand()"
	with oRs
		.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseclient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =			1 ' adLockReadOnly
		.Source =			strSql
		.Open
	end with

do while not oRs.EOF
	Response.Write oRs("strBookTitle") & " "
oRs.MoveNext
loop
%>