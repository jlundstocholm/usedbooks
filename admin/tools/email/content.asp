<!-- #include virtual="/admin/tools/toolbar.asp" -->
<h2 class="content-indent">Email til brugere af [usedbooks.dk]</h2>
<h3 class="content-indent" style="color:#ff0000;">Advarsel</h3>
<p class="content-indent" style="color:#ff0000;">
	Dette vil udsende en email til alle brugere af [usedbooks.dk].
</p>
<div class="content-indent">
	<form action="sendemail.asp" method="post">
		<table>
			<tr>
				<td>Type&nbsp;af&nbsp;email</td><td><input type="radio" name="emailtype" value="operations"/>Drift-email&nbsp;&nbsp;
													<input type="radio" name="emailtype" value="general"/>Generel email</td>
			</tr>
			<tr>
				<td>Emne</td><td><input type="text" size="60" name="subject"/></td>
			</tr>
			<tr>
				<td>Testudsending</td><td><select name="testemail"><option value="0">Denne email udsendes som test</option><option value="1" style="color:#ff0000;">Dette er en RIGTIG udsending til ALLE.</option></select></td>
			</tr>
			<tr>
				<td>Tekst</td><td>
								<textarea name="body" cols="60" rows="20"></textarea>
							  </td>
			</tr>
			<tr>
				<td>&nbsp;</td><td><input type="submit" value="Send email"/></td>
			</tr>
		</table>
	</form>
</div>
