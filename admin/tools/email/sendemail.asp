<%@Language="VBScript" %>
<%
option explicit

if not (session("logonValid") = true  AND Session("tintRole") = 1 )then
   Response.Redirect("/")
end if

%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.CacheControl = "no-cache"
Response.AddHeader "pragma","no-cache"

Server.ScriptTimeout = 600

dim oUserEmail
set oUserEmail = Server.CreateObject("ADODB.Recordset")
dim strType' as string

SELECT CASE Request.Form("emailtype")
	CASE "operations"
		call SendEmail("tintEmailOperations")
	CASE "general"
		call SendEmail("tintEmailGeneral")
END SELECT

'call SendEmail(1,1)

private sub SendEmail(strType)
	dim strSql	' as string
	strSql = "select strEmail,strFirstName,strLastName FROM ubdk_users"
	dim oRsEmail
	set orsEmail = Server.CreateObject("ADODB.Recordset")
	with oRsEmail
		.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseClient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =			1 ' adReadOnly
		.Source =			strSQL
		.Open
	end with
	
	dim oEmail				' as JMail.Message
	dim sBody,sFooter,sType	' as string
	sBody = Request.Form("body")
	
	SELECT CASE strType
		CASE "tintEmailOperations"
			sType = "Driftemails" 
		CASE "tintEmailGeneral"
			sType = "Generelle emails"
	END SELECT
	
	sFooter ="-- " & vbCrLf &_
			"Med venlig hilsen" & vbCrLf &_
			vbCrLf &_
			"Morten Ovi og Jesper Stocholm" & vbCrLf &_
			"http://www.usedbooks.dk" & vbCrLf &_
			vbCrLf &_
			"�nsker du ikke at modtage flere nyhedsbreve fra usedbooks.dk, kan du logge ind og �ndre det i din brugerprofil. Bem�rk at du kan v�re tilmeldt/afmeldt to forskellige nyhedsbreve, med hvert deres indhold. Dette nyhedsbrev har v�ret i kategorien """ & sType & """."
	
	sBody = "K�re %%strFirstName%%" & vbCrLf &_
			vbCrLf &_
			sBody & vbCrLf &_
			vbCrLf &_
			sFooter
	
	'Response.End
	
	set oEmail = Server.CreateObject("JMail.Message")
	
	oEmail.FromName = "usedbooks.dk"
	oEmail.From = "kontakt@usedbooks.dk"
	oEmail.Subject = Request.Form("subject")
	oEmail.Body = sBody
	oEmail.AddRecipient "%%strEmail%%", "%%strFirstName%% %%strLastName%%"
	
	dim oMerge
	set oMerge = Server.CreateObject("JMail.MailMerge")
	oMerge.MailTemplate = oEmail
	if CInt(Request.Form("testemail")) <> 1 then
		Response.Write "<h1>Dette er en test</h1>"
		Response.Write "<code>" & Replace(sBody,vbCrLf,"<br>") & "</code>"
		oMerge.SetDebugMode "admin@usedbooks.dk", 10
	else
		Response.Write "<h1>Denne email er blevet sendt ud til alle.</h1>"
		Response.Write "<code>" & Replace(sBody,vbCrLf,"<br>") & "</code>"
	end if
	
	
	oMerge.BulkMerge oRsEmail, false, "smtp.unoeuro.com"
	
end sub

'Response.Write Request.Form("testemail")

%>