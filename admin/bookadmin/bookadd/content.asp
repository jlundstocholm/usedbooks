<h1>Tilf�j ny bog</h1>
<div class="content-indent">
	For at registrere en bog p� usedbooks.dk skal du blot udfylde felterne
	herunder.<br/>
	<br/>
	Felter markeret med stjerne (<span class="asterisk-smoke">&#42;</span>) er obligatoriske. Bogen oprettes n�r du klikker p� "opret".
</div>
<br/>
<div class="content-indent">
<form method="post" action="create/default.asp" onsubmit="return CheckNewBook(this)">
<table class="contact">
    <tr>
		<td class="cell-center-vertical">&nbsp;Titel p� bog</td>
		<td width=10>&nbsp;<span class="asterisk">*</span></td>
		<td><input name = "Title" type = "text" size = "60" maxlength = "255"/></td>
	</tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;ISBN</td>
		<td width=10>&nbsp;</td>
		<td><input name = "isbn" type = "text" size = "25" maxlength = "30"/></td>
	</tr>

	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="font-style:italic;">ISBN skal angives uden bindestreger, dvs. ISBN &quot;0-861004-02-8&quot; skal angives som &quot;0861004028.&quot;.
		Hvis du angiver et ISBN-nummer, s� vil vi for engelske b�ger fors�ge at vise et billede af bogen ved siden af
		de oplysninger du opgiver.
		</td>
	</tr>

	<tr>
		<td class="cell-center-vertical">&nbsp;Pris</td>
		<td width=10>&nbsp;<span class="asterisk">*</span></td>
		<td><input name = "price" type = "text" size = "5" maxlength = "7" />&nbsp;kr.</td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Forfattere</td>
		<td width=10>&nbsp;</td>
		<td><input name = "authors" type = "text" size = "60" /></td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Udgivelses�r</td>
		<td width=10>&nbsp;</td>
		<td>
							<select name="yearpublic">
								<option value="0">intet/andet �r</option>
								<% dim intI 
								   for intI = 1990 to 2006
								%>
								<option value="<% = intI %>"><% = intI %></option>
								<%
								   next
								%>
							</select>
						 </td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Udgave</td>
		<td width=10>&nbsp;</td>

		<td>
							<select name="edition">
								<% do while not oRsEditions.EOF %>
								<option value="<% = oRsEditions("idEdition") %>"><% = oRsEditions("strEdition") %></option>
								<% oRsEditions.MoveNext
								   loop
								%>
							</select>
					   </td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Kursus</td>
		<td width=10>&nbsp;</td>
		<td><input type="text" name="course" size="5" maxlength="5" /></td>
      </tr>
		
	  <tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="font-style:italic;">
			Husk et evt. foranstillet 0. Et kursus skal alts� angives som &quot;02335&quot; og 
			ikke &quot;2335&quot;. �nsker du at s�tte en lommeregner til salg, skal du som kursus
			skrive &quot;LOMM&quot;</td>
	  </tr>
		
	<tr>
		<td class="cell-center-vertical">&nbsp;Bogens&nbsp;status</td>
		<td width=10>&nbsp;</td>
		<td>
								<select name="status">
									<option value="s">Bogen er til salg</option>
									<option value="b">Bogen �nskes k�bt</option>
								</select>
							  </td>
    </tr>

	<tr>
		<td class="cell-center-vertical">&nbsp;Bogens&nbsp;stand</td>
		<td width=10>&nbsp;</td>
		<td>
								<select name="condition" ID="Select1">
									<% do while not oRsCondition.EOF %>
									<option value="<% = oRsCondition("id") %>"><% = oRsCondition("strCondition_dk") %></option>
									<% oRsCondition.MoveNext
									   loop
									%>
								</select>
		</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="font-style:italic;">Husk at v�lge en v�rdi for bogens stand - det er en
		nem og hurtig m�de for potentielle k�bere at v�lge lige netop din bog.</td>
	</tr>

    <tr>
		<td>&nbsp;Evt.&nbsp;kommentar</td>
		<td width="10">&nbsp;</td>
		<td>
								<textarea name="txtComment" cols="50" rows="10"></textarea>
							</td>
    </tr>
    <tr>
		<td colspan="3"><input type = "submit" value = "Opret"/><input type = "reset" value = "Nulstil formular"/></td>
    </tr>
</table>
</form>
</div>