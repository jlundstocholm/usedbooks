<%@Language="VBScript"%>
<%
option explicit

if not Session("logonValid") then
	Response.Redirect("/")
end if

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.Expires = 0
%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
strSql = "SELECT idEdition,strEdition FROM ubdk_editions ORDER BY idEdition ASC"
dim oRsEditions ' as ADODB.Recordset
set oRsEditions = Server.CreateObject("ADODB.Recordset")
with oRsEditions
		.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseclient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =			1 ' adLockReadOnly
		.Source =			strSql
		.Open
end with

strSql = "SELECT id,strCondition_dk FROM ubdk_bookcondition ORDER BY id ASC"
dim oRsCondition	' as ADODB.Recordset
set oRscondition = Server.CreateObject("ADODB.Recordset")
with oRsCondition
.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseclient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =			1 ' adLockReadOnly
		.Source =			strSql
		.Open
end with

%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>