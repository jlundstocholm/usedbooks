<%@Language="VBScript"%>
<%
option explicit

if not Session("logonValid") then
	Response.Redirect("/")
end if

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.Expires = 0
%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
dim strMessage,strTitle,strAuthors,strPublisher,strCourseId,cBookStatus,txtComment ' as string
dim lngIsbn ' as long
dim intPrice,intYearPublic,intPages,intEdition,intBookCondition ' as integer
dim validInput ' as boolean

strTitle = Replace(Request.Form("Title"),"'","''")
lngIsbn = Request.Form("isbn")
intPrice = Request.Form("price")
strAuthors = Replace(Request.Form("authors"),"'","''")
intYearPublic = Request.Form("yearpublic")
intEdition = Request.Form("edition")
strCourseID = Replace(Request.Form("course"),"'","''")
cBookStatus = Request.Form("status")
intBookCondition = Request.Form("condition")
txtComment = Replace(Request.Form("txtComment"),"'","''")
validInput = false

public function validateInput
	validateInput = true
	if Len(strTitle) = 0 OR Len(strTitle) > 255 then
		validateInput = false
		strMessage = "Titlen p� din bog er obligatorisk."
		exit function
	end if
	
	if Len(lngIsbn) <> 0 then
		'if isNumeric(lngIsbn) then
			if not (Len(lngIsbn) = 10 OR Len(lngIsbn) = 13) then
				validateInput = false
				strMessage = "ISBN passer ikke. Det skal angives som enten 10 cifre eller 9 cifre og et X. Alternativt kan det angives som 13 cifre eller 12 cifre og et X."
				exit function
			end if
		'else
		'	validateInput = false
		'	strMessage = "ISBN er ikke korrekt. Det skal angives som enten 10 eller 13 cifre og uden bindestreger."
		'	exit function
		'end if
	end if
	
	if Len(intPrice) > 0 then
		on error resume next
			intPrice = CInt(intPrice)
			If Err.number <> 0 then
				validateInput = false
				strMessage = "Din pris skal angives som et heltal"
				exit function
			end if
		on error goto 0
	else
		validateInput = false
		strMessage = "Din pris skal angives som et heltal"
		exit function
	end if
	
	if Len(strAuthors) > 255 then
		validateInput = false
		strMessage = "Din liste over forfattere m� ikke overstige 255 tegn."
		exit function
	end if
	
	
	if Len(intYearPublic) > 0 then
		if not isNumeric(intYearPublic) then
			validateInput = false
			strMessage = "Udgivelses�ret skal enten ikke v�lges eller v�re imellem 1950 og 2003."
			exit function
		end if
		
		intYearPublic = CInt(intYearPublic)
		if intYearPublic < 1950 AND intYearPublic > 2003 then
			validateInput = false
			strMessage = "Udgivelses�ret skal enten ikke v�lges eller v�re imellem 1950 og 2003."
			exit function
		end if
	end if
	
	if Len(intEdition) > 0 then
		if not isNumeric(intEdition) then
			validateInput = false
			strMessage = "Udgave skal enten v�lges som &quot;Ingen udgave&quot; eller en af de andre v�rdier i menuen."
			exit function
		end if
		
		intEdition = CInt(intEdition)
		if intEdition < 0 AND intEdition > 12 then
			validateInput = false
			strMessage = "Udgave skal enten v�lges som &quot;Ingen udgave&quot; eller en af de andre v�rdier i menuen."
			exit function
		end if
	end if
	
	if Len(strCourseId) > 0 then
		if Cstr(strCourseId) = "10011" then
			strCourseId = "10010"
		end if
		
		'Response.Write strCourseId
		
		dim oRsCourses ' as ADODB.Recordset
		dim strSqlCourse ' as string
		strSqlCourse = "SELECT COUNT(*) As CountOfCourses FROM ubdk_coursedetails WHERE strCourseId = '" & strCourseId & "'"
		set oRsCourses = Server.CreateObject("ADODB.Recordset")
		with oRsCourses
			.ActiveConnection =	oCon
			.CursorLocation =	3 ' adUseclient
			.CursorType =		0 ' adOpenForwardOnly
			.LockType =			1 ' adLockReadOnly
			.Source =			strSqlCourse
			.Open
		end with
	
		if CInt(oRsCourses("CountOfCourses")) = 0 then
			validateInput = false
			strMessage = "Det kursus du angiver <b>(" & strCourseId & ")</b> er ikke i listen over kurser p� DTU."
		end if
	end if
	
	if not (cBookStatus = "s" or cBookStatus = "b") then
		validateInput = false
		strMessage = "Du skal v�lge enten at angive din bog som &quot;Til salg&quot; eller &quot;�nskes k�bt&quot;"
		exit function
	end if
	
	if not isNumeric(intBookCondition) then
		validateInput = false
		strMessage = "Bogens stand skal v�lges som enten &quot;Ikke angivet&quot; eller en af de andre v�rdier i menuen."
	else
		if CInt(intBookCondition) > 5 then
			validateInput = false
			strMessage = "Bogens stand skal v�lges som enten &quot;Ikke angivet&quot; eller en af de andre v�rdier i menuen."
		end if
	end if
		
end function

if validateInput then
	call createSql	
end if

private sub createSql
	strSql = "INSERT INTO ubdk_books (bookid,userId,strBookTitle,intPrice,intYearOfPublication,tintEdition,dtInsertDate,buyOrSell,tintCondition"
	
	if Len(lngIsbn) > 0 then
		strSql = strSql & ",isbn"
	end if
	
	if Len(strAuthors) > 0 then
		strSql = strSql & ",strAuthorName"
	end if
	
	if Len(strCourseId) > 0 then
		strSql = strSql & ",intCourseId"
	end if
	
	if Len(txtComment) > 0 then
		strSql = strSql & ",txtComment"
	end if
	
	Dim oGuid	' as Scriptlet.TypeLib
	Dim sGuid	' as string
	Set oGuid = Server.CreateObject("Scriptlet.TypeLib")
	sGuid = Left(Trim(oGuid.GUID),38)
	
	strSql = strSql & ") VALUES ('" & sGuid & "','" & Session("userId") & "','" & strTitle & "'," &_
					intPrice & "," & intYearPublic & "," & intEdition & ","&_
					"Now() ,'" & cBookStatus & "'," & intBookCondition
	if Len(lngIsbn) > 0 then
		strSql = strSql & ",'" & lngIsbn & "'"
	end if
	
	if Len(strAuthors) > 0 then
		strSql = strSql & ",'" & strAuthors & "'"
	end if
	
	if Len(intPages) > 0 then
		strSql = strSql & "," & intPages
	end if
	
	if Len(strPublisher) > 0 then
		strSql = strSql & ",'" & strPublisher & "'"
	end if
	
	if Len(strCourseId) > 0 then
		dim strSqlCourseId
		dim oRsCourseId ' as ADODB.Recordset
		strSqlCourseId = "SELECT id FROM ubdk_coursedetails WHERE strCourseId = '" & strCourseId & "'"
		'response.Write strSqlCourseId
		set oRsCourseId = Server.CreateObject("ADODB.Recordset")
		with oRsCourseId
			.ActiveConnection =	oCon
			.CursorLocation =	3 ' adUseclient
			.CursorType =		0 ' adOpenForwardOnly
			.LockType =			1 ' adLockReadOnly
			.Source =			strSqlCourseId
			.Open
		end with
		
		strSql = strSql & "," & oRsCourseId("id")
		
	end if

	if Len(txtComment) > 0 then
		strSql = strSql & ",'" & txtComment & "'"
	end if
	
	strSql = strSql & ")"
	'response.Write strSql				

	call doQuery(strSql,lngIsbn,sGuid)
end sub

public sub doQuery(strSql,lngIsbn,sGuid)
	oCon.Execute(strSql)
	if Len(lngIsbn) > 0 then
		call UpdateBooks(sGuid,lngIsbn)
	end if
	validInput = true
end sub

private sub UpdateBooks(Guid,lngIsbn)
	dim sSql	' as string
	dim oRsIsbn ' as ADODB.Recordset
	sSql = "SELECT COUNT(bookId) As NumberOfBooks FROM ubdk_books WHERE ISBN = '" & lngIsbn & "' AND tintHasImageOnAmazon = 1 AND tintAmazonChecked = 1"
	
	'Response.Write sSql
	set oRsIsbn= Server.CreateObject("ADODB.Recordset")
	with oRsIsbn
		.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseclient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =			1 ' adLockReadOnly
		.Source =			sSql
		.Open
	end with
	
	dim intBooks	'as integer
	intBooks = CInt(oRsIsbn("NumberOfBooks"))
	'Response.Write intBooks
	if intBooks > 0 then
		'Response.Write "jesp"
		sSql = "UPDATE ubdk_books SET tintHasImageOnAmazon = 1, tintAmazonChecked = 1 WHERE bookId = '" & Guid & "'"
		Response.Write sSql
		oCon.Execute(sSql)
	end if
end sub


%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>