<h2 class="content-indent">B�ger</h2>
<div class="content-indent"><% = strMessage %></div>
<% if bQueryDb then %>
<table width="560" style="margin-left: auto; margin-right: auto;border-left: black 1px dashed;border-right: black 1px dashed;border-top: black 1px solid;border-bottom: black 1px solid" id="Table3">
	<tr style="background: #7ba3de;">
		<td colspan="3">Bogdata</td>
	</tr>
	<tr>
		<td>Titel p� bog</td><td><% = oRsBooks("strBookTitle") %></td><td valign="top" rowspan="12"><img alt="" height="140" width="108" src="http://images-eu.amazon.com/images/P/<% = oRsBooks("ISBN") %>.02.MZZZZZZZ"/></td>
	</tr>
	<tr>
		<td>ISBN-nummer</td><td><% = oRsBooks("ISBN") %></td>
	</tr>
	<tr>
		<td>Prisid�</td><td><% = FormatCurrency(oRsBooks("intPrice"),2) %></td>
	</tr>
	<tr>
		<td>Forfattere</td><td><% = oRsBooks("strAuthorName") %></td>
	</tr>
	<tr>
		<td>Udgivelses�r</td><td><% = oRsBooks("intYearOfPublication") %></td>
	</tr>
	<tr>
		<td>Dato for oprettelse af bogen</td><td><% = FormatDateTime(oRsBooks("dtInsertDate"),vbLongDate) %></td>
	</tr>
	<tr>
		<td>Salg / k�b</td><td><% if oRsBooks("buyORsell") = "s" then Response.Write "Bogen er sat til salg" else Response.Write "Bogen �nskes" end if %></td>
	</tr>
	<tr>
		<td>Udgave</td><td><% = oRsBooks("strEdition") %></td>
	</tr>
	<tr>
		<td>Evt. relateret kursus</td><td><% = oRsBooks("strCourseId") & " " & oRsBooks("strCoursename") %></td>
	</tr>
	<tr>
		<td>Bogens status</td><td><% if CInt(oRsBooks("tintActive")) = 1 then Response.Write "Aktiv" else Response.Write "Slettet" end if  %></td>
	</tr>
	<tr>
		<td>Bogens stand</td><td><% = oRsBooks("strCondition_dk") %></td>
	</tr>
	<tr>
		<td>Kommentar</td><td><% if not isnull(oRsBooks("txtComment")) then Response.Write Replace(Server.HTMLEncode(oRsBooks("txtComment")),vbCrLf,"<br/>") end if %></td>
	</tr>
	<tr>
		<td colspan="3">
			<a href="/admin/bookadmin/bookedit/?id=<% = Server.URLEncode(oRsBooks("bookId")) %>&amp;checksum=<% = md5(application("initVector") & oRsBooks("bookId")) %>"><img src="/images/ret.jpg" alt="Ret data for denne bog" width="61" height="17" /></a>&nbsp;
			<% if CInt(oRsBooks("tintActive")) = 1 then %>
			<a onclick="return promptToDelete('<% = Server.URLEncode(oRsBooks("bookId")) %>','<% = md5(application("initVector") & oRsBooks("bookId") & "true") %>','true');" href="/admin/bookadmin/bookdelete/?id=<% = Server.URLEncode(oRsBooks("bookId")) %>&amp;checksum=<% = md5(application("initVector") & oRsBooks("bookId")) %>"><img src="/images/slet.jpg" alt="Slet denne bog" width="63" height="17" /></a>
			<% end if %>
		</td>
	</tr>
</table>
<% end if %>