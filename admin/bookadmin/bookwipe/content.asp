<% if not validateURI then %>
<h1>Der skete en fejl</h1>
<div class="content-indent">Det er ikke muligt for dig at slette den 
aktuelle bog. Hvis du ikke mener, at dette er korrekt, s� kontakt os
venligst via linksene menuen til venstre.<br/><br/>
Teamet bag usedbooks.dk</div>
<% end if %>