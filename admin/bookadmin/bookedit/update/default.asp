<%@Language="VBScript"%>
<%
option explicit

if not Session("logonValid") then
	Response.Redirect("/")
end if

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.Expires = 0
%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
dim strMessage,strTitle,strAuthors,strPublisher,strCourseId,cBookStatus,intBookCondition,strIsbn,txtComment ' as string
'dim lngIsbn ' as long

dim intPrice,intYearPublic,intPages,intEdition,tActive ' as integer
dim validInput ' as boolean

strTitle = Replace(Request.Form("Title"),"'","''")
strIsbn = Request.Form("isbn")
intPrice = Request.Form("price")
strAuthors = Replace(Request.Form("authors"),"'","''")
intYearPublic = Request.Form("yearpublic")
intPages = Request.Form("pages")
intEdition = Request.Form("edition")
strPublisher = Replace(Request.Form("publisher"),"'","''")
strCourseID = Replace(Request.Form("course"),"'","''")
cBookStatus = Request.Form("status")
tActive = Request.Form("delStat")
intBookCondition = Request.Form("condition")
'Response.Write intBookCondition
txtComment = Replace(Request.Form("txtComment"),"'","''")

validInput = false

public function validateInput
	'Response.Write "Beginning to validate input"
	validateInput = true
	if Len(strTitle) = 0 OR Len(strTitle) > 255 then
		validateInput = false
		strMessage = "Titlen p� din bog er obligatorisk."
		exit function
	end if
	
	if Len(strIsbn) <> 0 then
		'if isNumeric(strIsbn) then
			if not (Len(strIsbn) = 10 OR Len(strIsbn) = 13) then
				validateInput = false
				strMessage = "ISBN passer ikke. Det skal angives som enten 10 cifre eller 9 cifre og et X. Alternativt kan det angives som 13 cifre eller 12 cifre og et X."
				exit function
			end if
			strIsbn = UpdateIsbn(strIsbn)
	end if
	
	if Len(intPrice) > 0 then
		on error resume next
			intPrice = CInt(intPrice)
			If Err.number <> 0 then
				validateInput = false
				strMessage = "Din pris skal angives som et heltal"
				Response.Write "fejl i pris"
				exit function
			end if
		on error goto 0
	else
		validateInput = false
		strMessage = "Din pris skal angives som et heltal"
		exit function
	end if
	
	if Len(strAuthors) > 255 then
		validateInput = false
		strMessage = "Din liste over forfattere m� ikke overstige 255 tegn."
		exit function
	end if
	
	
	if Len(intYearPublic) > 0 then
		if not isNumeric(intYearPublic) then
			validateInput = false
			strMessage = "Udgivelses�ret skal enten ikke v�lges eller v�re imellem 1950 og 2003."
			exit function
		end if
		
		intYearPublic = CInt(intYearPublic)
		if intYearPublic < 1950 AND intYearPublic > 2003 then
			validateInput = false
			strMessage = "Udgivelses�ret skal enten ikke v�lges eller v�re imellem 1950 og 2003."
			exit function
		end if
	end if
	
	if Len(intEdition) > 0 then
		if not isNumeric(intEdition) then
			validateInput = false
			strMessage = "Udgave skal enten v�lges som &quot;Ingen udgave&quot; eller en af de andre v�rdier i menuen."
			exit function
		end if
		
		intEdition = CInt(intEdition)
		if intEdition < 0 AND intEdition > 12 then
			validateInput = false
			strMessage = "Udgave skal enten v�lges som &quot;Ingen udgave&quot; eller en af de andre v�rdier i menuen."
			exit function
		end if
	end if
	
	if Len(strCourseId) > 0 then
		if Cstr(strCourseId) = "10011" then
			strCourseId = "10010"
		end if
		
		'Response.Write strCourseId
		
		dim oRsCourses ' as ADODB.Recordset
		dim strSqlCourse ' as string
		strSqlCourse = "SELECT COUNT(*) As CountOfCourses FROM ubdk_coursedetails WHERE strCourseId = '" & strCourseId & "'"
		set oRsCourses = Server.CreateObject("ADODB.Recordset")
		with oRsCourses
			.ActiveConnection =	oCon
			.CursorLocation =	3 ' adUseclient
			.CursorType =		0 ' adOpenForwardOnly
			.LockType =			1 ' adLockReadOnly
			.Source =			strSqlCourse
			.Open
		end with
	
		if CInt(oRsCourses("CountOfCourses")) = 0 then
			validateInput = false
			strMessage = "Det kursus du angiver <b>(" & strCourseId & ")</b> er ikke i listen over kurser p� DTU."
		end if
	end if
	
	if not (cBookStatus = "s" or cBookStatus = "b") then
		validateInput = false
		strMessage = "Du skal v�lge enten at angive din bog som &quot;Til salg&quot; eller &quot;�nskes k�bt&quot;"
		exit function
	end if
	
	if not isNumeric(intBookCondition) then
		validateInput = false
		strMessage = "Bogens stand skal v�lges som enten &quot;Ikke angivet&quot; eller en af de andre v�rdier i menuen."
	else
		if CInt(intBookCondition) > 5 then
			validateInput = false
			strMessage = "Bogens stand skal v�lges som enten &quot;Ikke angivet&quot; eller en af de andre v�rdier i menuen."
		end if
	end if
		
end function

if validateInput then
	call createSql	
end if

private sub createSql

	strSql = "UPDATE ubdk_books set " &_
				"strBookTitle = '" & strTitle & "'" &_
				",intPrice = " & intPrice &_
				",intYearOfPublication = " & intYearPublic &_
				",tintEdition = " & intEdition &_
				",buyORsell = '" & cBookStatus & "'" &_
				",tintActive = " & tActive &_
				",tintCondition = " & intBookCondition
				
	
	if Len(strIsbn) > 0 then
		strSql = strSql & ",isbn = '" & UCase(strIsbn) & "'"
	else
		strSql = strSql & ",isbn = ''"
	end if
	
	if Len(strAuthors) > 0 then
		strSql = strSql & ",strAuthorName='" & strAuthors & "'"
	else
		strSql = strSql & ",strAuthorName=''"
	end if
	
	if Len(strCourseId) > 0 then
		dim strSqlCourseId
		dim oRsCourseId ' as ADODB.Recordset
		strSqlCourseId = "SELECT id FROM ubdk_coursedetails WHERE strCourseId = '" & strCourseId & "'"
		'response.Write strSqlCourseId
		set oRsCourseId = Server.CreateObject("ADODB.Recordset")
		with oRsCourseId
			.ActiveConnection =	oCon
			.CursorLocation =	3 ' adUseclient
			.CursorType =		0 ' adOpenForwardOnly
			.LockType =			1 ' adLockReadOnly
			.Source =			strSqlCourseId
			.Open
		end with
		'Response.Write oRsCourseId("id")
		
		'strSql = strSql & "," & oRsCourseId("id")
		strSql = strSql & ",intCourseId=" & oRsCourseId("id")
	end if
	
	if Len(txtComment) > 0 then
		strSql = strSql & ",txtComment='" & txtComment & "'"
	else
		strSql = strSql & ",txtComment = ''"
	end if
	
	strSql = strSql & " WHERE bookId = '" & Request.QueryString("id") & "' AND userId = '" & Session("userId") & "'"
	'Response.Write strSql

	'response.Write strSql				
	call doQuery(strSql)
end sub

public function UpdateIsbn(strIsbn)
	dim strSqlIsbn ' as string
	strSqlIsbn = "SELECT ISBN FROM ubdk_books WHERE bookId = '" & Request.QueryString("id") & "'"
	dim oRsIsbn ' as ADODB.Recordset
	set oRsIsbn = Server.CreateObject("ADODB.Recordset")
		with oRsIsbn
			.ActiveConnection =	oCon
			.CursorLocation =	3 ' adUseclient
			.CursorType =		0 ' adOpenForwardOnly
			.LockType =			1 ' adLockReadOnly
			.Source =			strSqlIsbn
			.Open
		end with
	if not strIsbn = oRsIsbn("ISBN") then
		dim strSqlIsbnUpdate ' as string
		strSqlIsbnUpdate = "UPDATE ubdk_books SET tintHasImageOnAmazon = 0, tintAmazonChecked = 0 WHERE bookId = '" & Request.QueryString("id") & "'"
		oCon.execute(strSqlIsbnUpdate)
	end if
	UpdateIsbn = strIsbn
end function

public sub doQuery(strSql)
	oCon.Execute(strSql)
	dim strURI
	strURI = "../default.asp?id=" & Request.QueryString("id") & "&checksum=" & Request.QueryString("checksum")
	'Response.Write strMessage
	'Response.Write validInput
	
	'Response.End
	Response.Redirect(strURI)
	validInput = true
end sub


%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>