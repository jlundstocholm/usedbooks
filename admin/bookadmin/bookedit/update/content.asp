<% if validateInput then %>
<h1>Bogens data er blevet rettet.</h1>
<div class="content-indent"><% = strMessage %></div>
<% else %>
<h1>Der skete en fejl</h1>
<div class="content-indent"><% = strMessage %></div>
<br/>
<div class="content-indent">Brug din browsers &quot;Tilbage-knap&quot; for at rette det.</div>
<% end if %>