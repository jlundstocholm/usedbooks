<% if validateURI(Request.QueryString("id"),Request.QueryString("checksum")) then %>
<h1>Ret i data for bog</h1>
<div class="content-indent">
	Felter markerede med asterisk (<span class="asterisk-smoke">&#42;</span>) er obligatoriske. Bogens data opdateres n�r du klikker p� "Opdat�r".
</div>
<br/>
<div class="content-indent">
	<form method="post" action="update/default.asp?id=<% = Request.QueryString("id")%>&checksum=<% = Request.QueryString("checksum") %>" onsubmit="return CheckNewBook(this)">
	<table class="contact" >
		<tr>
			<td class="cell-center-vertical">&nbsp;Titel p� bog</td>
			<td width=10>&nbsp;<span class="asterisk">*</span></td>
			<td><input value="<% = Server.HTMLEncode(oRsBook("strBookTitle")) %>" name="Title" type = "text" size = "60" maxlength = "255" /></td>
		</tr>
	    <tr>
			<td class="cell-center-vertical">&nbsp;ISBN</td>
			<td width=10>&nbsp;</td>
			<td><input value="<% = oRsBook("ISBN") %>" name = "isbn" type = "text" size = "25" maxlength = "30"/></td>
	    </tr>
	    <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><i>ISBN skal angives uden bindestreger, dvs. ISBN &quot;0-861004-02-8&quot; skal angives som &quot;0861004028&quot;</i></td>
	    </tr>
		<tr>
			<td class="cell-center-vertical">&nbsp;Pris</td>
			<td width=10>&nbsp;<span class="asterisk">*</span></td>
			<td><input value="<% = oRsBook("intPrice") %>" name = "price" type = "text" size = "5" maxlength = "7" />&nbsp;kr.</td>
	    </tr>
		<tr>
			<td class="cell-center-vertical">Forfattere</td>
			<td width=10>&nbsp;</td>
			<td><input value="<% if not Len(oRsBook("strAuthorName")) = 0 then Response.Write Server.HTMLEncode(oRsBook("strAuthorName")) end if %>" name = "authors" type = "text" size = "60" /></td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Udgivelses�r</td>
		<td width=10>&nbsp;</td>
		<td>
			<select name="yearpublic" >
				<option value="0">intet/andet �r</option>
				<% dim intI 
				   for intI = 1950 to 2006
				%>
				<option <% if oRsBook("intYearOfPublication") = intI then Response.Write "selected=""selected""" end if %> value="<% = intI %>" ><% = intI %></option>
				<%
				   next
				%>
			</select>
		 </td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Udgave</td>
		<td width=10>&nbsp;</td>
		<td>
			<select name="edition" id="Select2">
			<% do while not oRsEditions.EOF %>
			<option <% if oRsBook("tintEdition") = oRsEditions("idEdition") then response.Write "selected=""selected""" end if %> value="<% = oRsEditions("idEdition") %>"><% = oRsEditions("strEdition") %></option>
			<% oRsEditions.MoveNext
			   loop
			%>
		</select>
	</td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Kursus</td>
		<td width=10>&nbsp;</td>
		<td><input value="<% if not Len(oRsBook("strCourseId")) = 0 then Response.Write Server.HTMLEncode(oRsBook("strCourseId")) end if  %>" type="text" name="course" size="5" maxlength="5" /></td>
	</tr>

	    <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><i>Kursusnummeret skal v�re p� fem cifre.</i></td>
	    </tr>


	<tr>
		<td class="cell-center-vertical">&nbsp;K�b / Salg</td>
		<td width=10>&nbsp;</td>
		<td>
			<select name="status" id="Select3">
				<option value="s">Bogen er til salg</option>
				<option <% if oRsBook("buyORSell") = "b" then Response.Write "selected=""selected""" end if %>value="b">Bogen �nskes k�bt</option>
			</select>
        </td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Bogens&nbsp;status</td>
		<td width=10>&nbsp;</td>
		<td>
			<select name="delStat" id="Select1">
				<option value="1">Bogen er aktiv</option>
				<option <% if CInt(oRsBook("tintActive")) = 0 then Response.Write "selected=""selected""" end if %>value="0">Bogen er inaktiv</option>
			</select>
		  </td>
    </tr>
    <tr>
		<td class="cell-center-vertical">&nbsp;Bogens&nbsp;stand</td>
		<td width=10>&nbsp;</td>
		<td>
			<select name="condition" id="Select4">
			<% do while not oRsCondition.EOF %>
			<option <% if CStr(oRsBook("tintCondition")) = CStr(oRsCondition("id")) then response.Write "selected=""selected""" end if %> value="<% = oRsCondition("id") %>"><% = oRsCondition("strCondition_dk") %></option>
			<% oRsCondition.MoveNext
			   loop
			%>
		</select>
		  </td>
    </tr>
    <tr>
		<td>&nbsp;Evt.&nbsp;kommentar</td>
		<td width=10>&nbsp;</td>
		<td>
			<textarea name="txtComment" cols="50" rows="10" id="Textarea1"><% = oRsBook("txtComment") %></textarea>
		</td>
    </tr>
    <tr>
		<td colspan="3"><input type = "submit" value = "Opdat�r"/><input type = "reset" value = "Nulstil formular" /></td>
    </tr>
</table>
</form>
</div>
<% else %>
<h1>Der skete en fejl</h1>
<div class="content-indent">
	<% = strMessage %>
</div>
<% end if %>