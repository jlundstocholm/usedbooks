<% if not validateURI then %>
<h1>Der skete en fejl</h1>
<div class="content-indent">Det er ikke muligt for dig at gendanne denne bog. Hvis du mener at dette er en fejl, s� kontakt os
venligst via linket i menuen til venstre.<br/><br/>
Teamet bag usedbooks.dk</div>
<% end if %>