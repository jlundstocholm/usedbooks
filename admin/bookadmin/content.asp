<!-- #include virtual="/include/navlinks_user.asp" -->
<h2 class="content-indent">B�ger</h2>
<div class="content-indent">
	<a href="/admin/bookadmin/bookadd/"><img height="17" width="96" src="/images/tilfoej.jpg" alt="Tilf�j ny bog" /></a>
</div>
<br/>
<div class="content-indent">F�lgende b�ger st�r til k�b eller salg:</div>
<table width="560" class="result" id="Table1">
	<tr class="result">
		<td>Titel</td><td>Prisid�</td><td>Udgivelses�r</td><td>Status</td><td>Detaljer</td>
	</tr>
	<%  dim j
		j = 0
		do while not oRsBooks.EOF
		if CInt(oRsBooks("tintActive")) = 1 then 
		%>
	<tr>
		<td><img src="/images/<% = trim(oRsBooks("buyORsell")) %>.jpg" width="15" height="15" alt="Denne bog <% if trim(oRsBooks("buyORsell")) = "k" then Response.Write "�nskes k�bt" else Response.Write "er til salg" end if %>" />
			<% if Len(oRsBooks("strBookTitle")) < 50 then Response.Write Server.HTMLEncode(oRsBooks("strBookTitle")) else Response.Write Server.HTMLEncode(Left(oRsBooks("strBookTitle"),47)) & " ..." end if %></td>
		<td><% = FormatCurrency(oRsBooks("intPrice"),2) %></td>
		<td style="text-align:center;"><% if CInt(oRsBooks("intYearOfPublication")) > 0 then Response.Write oRsBooks("intYearOfPublication") %></td>
		<td style="text-align: center;">Aktiv</td>
		<td><a href="/admin/bookadmin/bookdetails/?id=<% = Server.URLEncode(oRsBooks("bookId")) %>&amp;checksum=<% = md5(application("initVector") & oRsBooks("bookId")) %>"><img src="/images/detaljer.jpg" alt="Se detaljer om den aktuelle bog" width="47" height="15" /></a></td>
	</tr>
	<%
			end if
			j = j + 1
			oRsBooks.MoveNext
			loop
	%>
	<%
		if j = 0 then
	%>
	<tr>
		<td colspan="5">...du har ingen b�ger registreret</td>
	</tr>
	<%
		end if
	%>
</table>

<div class="content-indent"><br/><br/>F�lgende b�ger er inaktive og vil derfor ikke fremg� ved en s�gning. 
Herunder kan du gendanne bogen hvis den ved en fejl er slettet, og du har ogs� mulighed for helt at slette bogen fra oversigten.
</div>
<table width="560" class="result" id="Table1">
	<tr class="result">
		<td>Titel</td><td width=70></td><td width=70></td>
	</tr>
	<%  dim count
		count = 0
		if not oRsBooks.BOF then
		oRsBooks.MoveFirst
		end if
		do while not oRsBooks.EOF
		if CInt(oRsBooks("tintActive")) = 0 then 
		%>
	<tr>
		<td><img	src="/images/<% = trim(oRsBooks("buyORsell")) %>.jpg" width="15" height="15" alt="Denne bog <% if trim(oRsBooks("buyORsell")) = "k" then Response.Write "�nskes k�bt" else Response.Write "er til salg" end if %>" />
			<% if Len(oRsBooks("strBookTitle")) < 50 then Response.Write Server.HTMLEncode(oRsBooks("strBookTitle")) else Response.Write Server.HTMLEncode(Left(oRsBooks("strBookTitle"),47)) & " ..." end if %></td>
		<td><a href="/admin/bookadmin/bookreactivate/?id=<% = Server.URLEncode(oRsBooks("bookId")) %>&amp;checksum=<% = md5(application("initVector") & oRsBooks("bookId")) %>"><img src="/images/gendan.jpg" alt="Gendan bogen" width="63" height="17" /></a></td>
		<td><a onclick="if (!confirm('Er du sikker p� at du vil slette bogen fra oversigten? Det vil ikke v�re muligt at genskabe den igen.')) return false;" href="/admin/bookadmin/bookwipe/?id=<% = Server.URLEncode(oRsBooks("bookId")) %>&amp;checksum=<% = md5(application("initVector") & oRsBooks("bookId")) %>"><img src="/images/slet.jpg" alt="Slet bogen helt" width="63" height="17" /></a></td>
	</tr>
	<%
			count = count + 1
			end if
			oRsBooks.MoveNext
			loop
			'end if
	%>
	<%
		if count = 0 then
	%>
	<tr>
		<td colspan="3">...du har ingen inaktive b�ger.</td>
	</tr>
	<%
		end if
	%>
</table>