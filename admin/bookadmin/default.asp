<%@Language="VBScript"%>
<%
option explicit

if not Session("logonValid") then
	Response.Redirect("/")
end if
%>
<!-- #include virtual="/utilities/md5.asp" -->
<%

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.CacheControl = "no-cache"
Response.AddHeader "pragma","no-cache"

%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
dim oRsBooks 
set oRsBooks = Server.CreateObject("ADODB.Recordset")
strSQL = "SELECT b.buyORsell, b.bookId, b.strBookTitle, b.intPrice, b.intYearOfPublication, b.dtInsertDate, b.tintActive FROM ubdk_books b WHERE userId = '" & Session("userId") & "' ORDER BY b.buyORsell DESC,strBookTitle ASC, dtInsertDate DESC"
with oRsBooks
	.ActiveConnection =	oCon
	.CursorLocation =	3 ' adUseClient
	.CursorType =		0 ' adOpenForwardOnly
	.LockType =			1 ' adReadOnly
	.Source =			strSQL
	.Open
end with

%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>

<%
oCon.Close
set oCon = nothing
%>