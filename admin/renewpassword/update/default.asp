<%@Language="VBScript"%>
<%
option explicit
%>
<!-- #include virtual="/utilities/md5.asp" -->

<%
'************************************************************
' d. 16-11-2002 kl 12.03: Taget i brug af Jesper Stocholm
'
'************************************************************

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.CacheControl = "no-cache"
Response.AddHeader "pragma","no-cache"
%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
dim strPswd,strPswdNew,strPswdConf,strMessage,strPswdDBMD5 ' as string
dim bInputValid ' as boolean
dim oRsConfPswd ' as ADODB.Recordset

set oRsConfPswd = Server.CreateObject("ADODB.Recordset")

strSql = "SELECT strPasswordMD5,userId FROM ubdk_users where userId = '" & Session("userId") & "'"
'response.Write strSql & "<br/>"
'response.end
with oRsConfPswd
	.ActiveConnection =	oCon
	.CursorLocation =	3 ' adUseclient
	.CursorType =		0 ' adOpenForwardOnly
	.LockType =		1 ' adLockReadOnly
	.Source =		strSql
	.Open
end with

strPswdDBMD5 = oRsConfPswd("strPasswordMD5")
'response.Write strPswdDBMD5

bInputValid = true
strPswd = Replace(Request.Form("PasswordOriginal"),"'","''")
strPswdNew = Replace(Request.Form("PasswordNew"),"'","''")
strPswdConf = Replace(Request.Form("PasswordConfirm"),"'","''")

private function validateNewPassword(strPswd,strPswdNew,strPswdConf,strPswdDBMD5)
	validateNewPassword = true
	
	if strPswdDBMD5 <> md5(strPswd) then
		strMessage = "Dit indtastede password og vores data i vores database stemmer ikke ovens med den emailadresse du loggede ind med."
		validateNewPassword = false
		exit function
	end if
	if strPswd = "" then
		strMessage = "Du skal indtaste dit nuv�rende password, som du evt har f�et pr. email."
		validateNewPassword = false
		exit function
	end if

	if strPswdNew = "" then
		strMessage = "Du skal v�lge et nyt password."
		validateNewPassword = false
		exit function
	end if

	if strPswdConf = "" then
		strMessage = "Du skal bekr�fte dit nye password."
		validateNewPassword = false
		exit function
	end if
	
	if strPswdNew <> strPswdConf then
		strMessage = "Dit nye password og din bekr�ftelse af det er ikke ens."
		validateNewPassword = false
		exit function
	end if
	
	if len(strPswdNew) < 4 then
		strMessage = "Dit nye password skal v�re p� mindst 4 tegn."
		validateNewPassword = false
		exit function
	end if
end function

if validateNewPassword(strPswd,strPswdNew,strPswdConf,strPswdDBMD5) then
	strSql = "UPDATE ubdk_users SET strPasswordMD5 = '" & md5(strPswdNew) & "', tintPasswordUseOnce = 0 WHERE userId = '" & Session("userId") & "'"
	'response.write strSql
	oCon.execute(strSql)
end if

%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>