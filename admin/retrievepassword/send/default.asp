<%@Language="VBScript"%>
<%
option explicit
%>
<!-- #include virtual="/utilities/md5.asp" -->
<%


Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.CacheControl = "no-cache"
Response.AddHeader "pragma","no-cache"
%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
dim strEmail ' as string
dim bValidEmail ' as boolean
strEmail = Replace(Request.Form("emailadresse"),"'","''")
bValidEmail = true

' F�rst skal der checkes om emailadressen findes i brugerdatabasen (fra login/default.asp)

dim strSqlGet,strMessage,fornavn
	strSqlGet = "SELECT strFirstName,userId FROM ubdk_users where tintActive = 1 AND strEmail = '" & strEmail & "'"
	strMessage = strSqlGet
	dim oRsGet ' as ADODB.Recordset
	set oRsGet = Server.CreateObject("ADODB.Recordset")
	with oRsGet
		.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseclient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =		1 ' adLockReadOnly
		.Source =		strSqlGet
		.Open
	end with
	
	if oRsGet.EOF AND oRsGet.BOF then
		bValidEmail = false
		strMessage = "Der findes ikke nogen aktive brugere med emailadressen '" & strEmail & "' i vores database. Dette betyder h�jest sandsynligt at brugeren med denne emailadresse har registreret sig, men har ikke trykket p� registreringslinket i den tilsendte email efter registrering."
		'Response.write "<h1>Fejl</h1><div>Der findes ikke nogen bruger med emailadressen '" & strEmail & "' i vores database.<br/><br/>Problemer? Kontakt os gerne (brug menupunktet til venstre).<br/><br/>Venlig hilsen<br/>usedbooks.dk</div>"
	else
		
		dim strPswd
		strPswd = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

		dim intI,j
		dim strTemp

			strTemp = ""
			j = 0
			for intI = 0 to 6
				Randomize
				j = 1 + int(61.0*Rnd())
				strTemp = strTemp & mid(strPswd,j,1)
			next

		dim mail,body
		set mail = Server.CreateObject("JMail.SMTPMail")
		'set mail = Server.CreateObject("JMail.Message")
		fornavn = oRsGet("strFirstName")

		body = "Hej " & fornavn & vbCrLf & vbCrLf & "Du modtager denne mail fordi der p� usedbooks.dk er bestilt" & vbCrLf & "et nyt password til brugeren med emailadressen '" & strEmail & "'." & vbCrLf & vbCrLf & "Dit nye password er:" & vbCrLf & vbCrLf & strTemp & vbCrLf & vbCrLf & "Bem�rk at du skal �ndre dette password f�rste gang du logger" & vbCrLf & "ind p� usedbooks.dk." & vbCrLf & vbCrLf & "Venlig hilsen" & vbCrLf & vbCrLf & "usedbooks.dk" & vbCrLf & "http://usedbooks.dk"

		With mail
			.Subject = "Password til usedbooks.dk"
			.Body = body
			.Sender = "support@usedbooks.dk"
			.SenderName = "usedbooks.dk"
			.AddRecipient strEmail
			'.ServerAddress = "mail.jirty.dk"
			'.nq
			.ServerAddress = Application("smtpserver")
			.Execute
		End With
	
		
		strSqlGet = "UPDATE ubdk_users SET tintPasswordUseOnce = 1, strPasswordMD5 = '" & md5(strTemp) & "' WHERE userId = '" & oRsGet("userId") & "'"
		oCon.Execute(strSqlGet)
	
		strMessage = "Et nyt password er sendt til '" & strEmail & "'. Husk at dette er et engangspassword og at du derfor bliver bedt om at lave et nyt password ved login."

	end if

Set mail = Nothing
%>

<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>