<h1>Fejl ved kontakt til k�ber/s�lger</h1>

<h2 class="content-indent">Hvorfor?</h2>
    <div class="content-indent">
    Du er har f�et et link til denne side da der er sket en fejl i forbindelse med 
	en person du har kontaktet i henhold til et k�b/salg af en bog. Fejlen er ikke din.
    </div>
<h2 class="content-indent">L�sning</h2>
<%'    <div class="content-indent">
   ' I henvisningen til denne side var en indikation af hvilken fejl det drejer sig om. 
	'L�sninger til fejlen er som f�lger:<br /> %>
	<% if errorID = "a" then %>
	<ul>
		<li>
			Personens emailadresse er "over quota" hvilket betyder at personen pt. har for mange
			emails p� sin konto, og derfor ikke kan modtage flere emails. L�sningen her er at
			pr�ve at sende vedkommende en email igen - typisk i l�bet af en til flere dage. <br />
			<% if phoneID <> "" then %>Personen har dog oplyst sit telefonnummer, s� pr�v at 
			kontakte vedkommende p� <%response.Write(phoneID) %>.<% end if %>
		</li>
	</ul>
	<% end if %>
	<% if errorID = "b" then %>
	<ul>
		<li>
			Personens emailadresse eller mailserver har midlertidige problemer. L�sningen her er at
			pr�ve at sende vedkommende en email igen - typisk i l�bet af en til flere dage.
		</li>
	</ul>
	<% end if %>
	<% if errorID = "c" then %>
	<ul>
		<li>
			Personens emailadresse er nedlagt. Vi har lukket personens konto, s� dennes b�ger ikke
			vil fremg� af sitet l�ngere<% if phoneID="" then %>, men der er desv�rre ikke nogen m�de at kontakte personen p�.
			<% else %>. Personen har dog oplyst sit telefonnummer, s� pr�v at kontakte vedkommende p� <%response.Write(phoneID) %>. <% end if %>
		</li>
	</ul>
	<% end if %>
	</div>