<%
	if oRsBooks.EOF AND oRsBooks.BOF then
		bQueryDb = false
		strMessage = "Bogen du s�ger findes ikke l�ngere i vores system."
	end if
%>
<h2 class="content-indent">B�ger</h2>
<% if bQueryDb then %>
<table width="560" class="content-indent">
	<tr>
		<td style="text-align: right;">Bogen er blevet vist <% = CInt(oRsBooks("intNumberOfViews")) + 1 %> gang<% if CInt(oRsBooks("intNumberOfViews"))>0 then 
		Response.Write "e." 
		else
		Response.Write "."
		end if
		%>
		</td>
	</tr>
</table>

<table width="560" class="result" id="Table6">
	<tr class="result">
		<td colspan="3">Bogdata</td>
	</tr>
	<tr>
		<td>Titel p� bog</td><td><% = oRsBooks("strBookTitle") %></td>
		<td valign="top" rowspan="12">
			<table class="contact">
				<tr class="contact">
					<td colspan="2">Kontakt</td>
				</tr>
				<tr>
					<td>Navn: </td><td><% = Replace(Trim(oRsBooks("strFirstName")) & "&nbsp;" &  Trim(oRsBooks("strLastName"))," ","&nbsp;") %></td>
				</tr>
				<% if CInt(oRsBooks("tintPublicAddress")) = 1 then %>
				<tr>
					<td>Telefon: </td><td><% = oRsBooks("strPhoneNumber") %></td>
				</tr>
				<tr>
					<td>Mobil: </td><td><% if not oRsBooks("intMobile") ="0" then Response.Write oRsBooks("intMobile") %></td>
				</tr>
				<% end if %>
				<tr>
					<td colspan="2"><a class="contact" title="Hvis kontaktpersonen ikke har �nsket at have sin adresse offentliggjort, s� kan du kontakte vedkommende her." href="/books/contact/?id=<% = Server.URLEncode(oRsBooks("bookId")) %>&amp;firstname=<% = Server.URLEncode(Trim(oRsBooks("strFirstName"))) %>&amp;lastname=<% = Server.URLEncode(Trim(oRsBooks("strLastName")))%>&amp;checksum=<% = md5(application("initVector") & oRsBooks("bookId") & Trim(oRsBooks("strFirstName")) & Trim(oRsBooks("strLastName")))%>">Direkte kontakt</a></td>
				</tr>
			</table>
			<br/>
			<% if CInt(oRsBooks("tintHasImageOnAmazon")) = 1 then %>
			<img class="result" alt="" height="140" width="108" src="http://images-eu.amazon.com/images/P/<% = oRsBooks("ISBN") %>.02.MZZZZZZZ"/>
			<% else %>
			<img class="result" alt="Intet billede til denne bog" height="140" width="108" src="/images/intet-billede.png"/>
			<% end if %>
		</td>
	</tr>
	<tr>
		<td>ISBN-nummer</td><td><% = oRsBooks("ISBN") %></td>
	</tr>
	<tr>
		<td>Prisid�</td><td><% = FormatCurrency(oRsBooks("intPrice"),2) %></td>
	</tr>
	<tr>
		<td>Forfattere</td><td><% = oRsBooks("strAuthorName") %></td>
	</tr>
	<tr>
		<td>Udgivelses�r</td><td><% if CInt(oRsBooks("intYearOfPublication")) = 0 then Response.Write "* intet �r *" else Response.Write oRsBooks("intYearOfPublication") end if %></td>
	</tr>
	<tr>
		<td>Dato&nbsp;for&nbsp;oprettelse&nbsp;af&nbsp;bogen</td><td><% = FormatDateTime(oRsBooks("dtInsertDate"),vbLongDate) %></td>
	</tr>
	<tr>
		<td>Salg&nbsp;/&nbsp;k�b</td><td><% if oRsBooks("buyORsell") = "s" then Response.Write "Bogen er sat til salg." else Response.Write "Bogen �nskes <span class='asterisk-smoke'>k�bt</span>." end if %></td>
	</tr>
	<tr>
		<td>Udgave</td><td><% = oRsBooks("strEdition") %></td>
	</tr>
	<tr>
		<td>Evt.&nbsp;relateret&nbsp;kursus</td><td><% = oRsBooks("strCourseId") & " " & oRsBooks("strCoursename") %></td>
	</tr>
	<tr>
		<td>Bogens stand</td><td><% = oRsBooks("strCondition_dk") %></td>
	</tr>
	<tr>
		<td>Kommentar</td><td><% if not isnull(oRsBooks("txtComment")) then Response.Write Replace(Server.HTMLEncode(oRsBooks("txtComment")),vbCrLf,"<br/>") end if %></td>
	</tr>
</table>
<p class="content-indent" style="text-align: center;">Direkte link til denne side: <a href="http://usedbooks.dk/redirect?id=<% = Clng(Application("blurint") + CLng(oRsBooks("id"))) %>">http://usedbooks.dk/redirect?id=<% = Clng(Application("blurint") + CLng(oRsBooks("id"))) %></a></p>
<% '<p class="content-indent" style="text-align:center;"><script type="text/javascript" src="http://impdk.tradedoubler.com/imp/pool/js/10472/552765"></script></p>%>
<% else %>
	<div class="content-indent"><% = strMessage %></div>
<% end  if %>