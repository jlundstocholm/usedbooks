<%@Language="VBScript"%>
<%
option explicit
%>
<!-- #include virtual="/utilities/md5.asp" -->
<%

'************************************************************
' d. 16-11-2002 kl 12.03: Taget i brug af Jesper Stocholm
' d. 18-11-2002 kl 10.20: Afleveret af Jesper Stocholm
' d. 23-11-2002 kl 11.01: Taget i brug af Jesper Stocholm
'
'************************************************************

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.CacheControl = "no-cache"
Response.AddHeader "pragma","no-cache"
%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
dim bListFormular ' as boolean
dim strMessage ' as string
if Trim(Request.QueryString("checksum")) = md5(application("initVector") & Trim(Request.QueryString("id")) & Trim(Request.Querystring("firstname")) & Trim(Request.Querystring("lastname"))) then
	bListFormular = true
	strMessage = "Fra nedenst�ende formular kan du skrive direkte til kontaktpersonen for bogen. Der vil blive tilsendt en email til kontaktpersonen baseret p� nedenst�ende felter. Bem�rk at du ikke beh�ver at skrive navnet p� bogen, da dette automatisk bliver medsendt. Udfyld et telefonnummer og/eller en emailadresse."
else
	bListFormular = false
	strMessage = "De data du overf�rer er forkerte. V�r opm�rksom p�, at du ikke m� rette i den URI, der f�rer til denne side."
end if
%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>