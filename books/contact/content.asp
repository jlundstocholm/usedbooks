<h1>Kontakt</h1>
<div class="content-indent"><% = strMessage %></div>
<br/>
<% if bListFormular then %>
<div class="content-indent">
	<form onsubmit="return checkContactFormular(document.forms.fContact.strName.value,document.forms.fContact.strPhone.value,document.forms.fContact.strMobilePhone.value,document.forms.fContact.strEmail.value,document.forms.fContact.strMessage.value);" id="fContact" action="send/default.asp?id=<% = Request.QueryString("id") %>&amp;checksum=<% = md5(application("initVector") & Request.QueryString("id"))%>" method="post">
		<table>
			<tr>
				<td class="cell-center-vertical">Til:</td><td><% = Request.QueryString("FirstName") & " " & Request.QueryString("LastName") %></td>
			</tr>
			<tr>
				<td class="cell-center-vertical">Dit navn</td><td><input value="<% Response.write Server.HTMLEncode(Session("strFirstName")) & " " & Server.HTMLEncode(Session("strLastName")) %>" type="text" size="30" name="strName" id="strName"/></td>
			</tr>
			<tr>
				<td class="cell-center-vertical">Tlf.nr.</td><td><input value="<% 
				if not Session("strPhone") = "" then
				Response.write Server.HTMLEncode(Session("strPhone"))
				else
				Response.write ""
				end if
				%>" type="text" size="8" name="strPhone"/></td>
			</tr>
			<tr>
				<td class="cell-center-vertical">Mobilnr.</td><td><input value="<% 
				if not Session("strMobile") = "" then
				Response.write Server.HTMLEncode(Session("strMobile")) 
				else
				Response.write ""
				end if
				%>" type="text" size="8" name="strMobilePhone"/></td>
			</tr>
			<tr>
				<td class="cell-center-vertical">Emailadresse</td><td><input value="<% 
				if not Session("strEmail") = "" then
				Response.write Server.HTMLEncode(Session("strEmail"))
				else
				Response.write ""
				end if
				%>" type="text" size="30" name="strEmail" id="strEmail"/></td>
			</tr>
			<tr>
				<td>Besked:</td>
				<td>
					<textarea name="strMessage" cols="50" rows="10"></textarea>
				</td>
			</tr>
			<tr>
				<td></td><td><input type="submit" value="Afsend besked" /></td>
			</tr>
		</table>
	</form>
</div>
<% end if %>