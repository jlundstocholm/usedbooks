<% if doSend then %>
<h1>Din besked er blevet sendt.</h1>
<div class="content-indent">Din besked er blevet afsendt til kontaktpersonen for den bog du har kigget p�.</div>
<% else %>
<h1>Din besked er ikke blevet sendt.</h1>
<div class="content-indent"><% = strMessage %></div>
<% end if %>