<%@Language="VBScript"%>
<%
option explicit
%>
<!-- #include virtual="/utilities/md5.asp" -->
<%

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.CacheControl = "no-cache"
Response.AddHeader "pragma","no-cache"
%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
dim bListFormular	' as boolean
dim strMessage		' as string

private function doSend ' as boolean
	dim bDoSend			' as boolean
	dim strId			' as string
	dim strCheckSumUri	' as string
	dim strCheckSumCalc	' as string
	bDoSend = true
	strId = Trim(Request.QueryString("id"))
	strCheckSumUri = Trim(Request.QueryString("checksum"))
	strCheckSumCalc = md5(application("initVector") & strId)
	if not strCheckSumUri = strCheckSumCalc then
		strMessage = "De data du overf�rer er forkerte. V�r opm�rksom p�, at du ikke m� rette i den URI, der f�rer til denne side."
		doSend = false
		exit function
		
	end if
	
	if Replace(Request.Form("strName"),"'","''") = "" then
		strMessage = "Du skal angive et navn for at kunne bruge kontaktformularen."
		doSend = false
		exit function
	end if
	
	if Replace(Request.Form("strMessage"),"'","''") = "" then
		strMessage = "Du skal skrive en besked for at kunne bruge kontaktformularen."
		doSend = false
		exit function
	end if
	
	if Replace(Request.Form("strPhone"),"'","''") = "" AND Replace(Request.Form("strMobilePhone"),"'","''") = "" AND Replace(Request.Form("strEmail"),"'","''") = "" then
		strMessage = "Du skal angive enten en emailadresse eller et telefonnummer for at kunne bruge kontaktformularen."
		doSend = false
		exit function
	end if
	
	doSend = bDoSend
end function

if doSend then 
	call sendEmail(Trim(Request.QueryString("id")))
	'call updateDB(Trim(Request.QueryString("id")))
else
end if

private sub sendEmail(strId)
	'dim msg			' as JMail.SMTPMail
	dim oRsEmail	' as ADODB.Record
	dim strSqlEmail	' as string
	'set msg = Server.CreateObject("JMail.SMTPMail")
	set oRsEmail = Server.CreateObject("ADODB.Recordset")
	strSqlEmail = "SELECT u.strFirstName,u.strLastName,u.strEmail,b.strBookTitle FROM ubdk_users u INNER JOIN ubdk_books b on b.userId = u.userId WHERE b.bookId = '" & strId & "'"
	with oRsEmail
		.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseClient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =			1 ' adLockReadOnly
		.Source =			strSqlEmail
		.Open
	end with
	
	dim FirstName	' as string
	dim LastName	' as string
	dim Email	' as string
	dim Title	' as string
	
	dim formName    ' as string
	dim formPhone   ' as string
	dim formMobilePhone   ' as string
	dim formEmail   ' as string
	dim formMessage ' as string

	FirstName = oRsEmail("strFirstName")
	LastName = oRsEmail("strLastName")
	Email = oRsEmail("strEmail")
	Title = oRsEmail("strBookTitle")

	formName = Request.Form("strName")
	formPhone = Request.Form("strPhone")
	formMobilePhone = Request.Form("strMobilePhone")
	formEmail = Request.Form("strEmail")
	formMessage  = Request.Form("strMessage")

	dim mail,body
	'set mail = Server.CreateObject("JMail.SMTPMail")
	dim msg
	'set msg = Server.CreateObject("JMail.Message")
	set msg = Server.CreateObject("JMail.SMTPMail")
	
	body = "Hej " & FirstName & VbCrLF & VbCrLf & formName & " har kontaktet dig i forbindelse med dit opslag p� usedbooks.dk med bogen '" & Title & "'." & vbCrLF & vbCrLf & "Du kan kontakte " & formName & " p�:" & vbCrLf & "Email: mailto:" & formEmail & vbCrLf & "Tlf.: " & formPhone & VbCrLF & "Mobil: " & formMobilePhone & vbCrLf & vbCrLf & "Herunder er beskeden fra " & formName & ":" & vbCrLf & vbCrLf & "--------- Start besked ---------" & vbCrLf & formMessage  & vbCrLf & vbCrLf & "--------- Slut besked ----------"  & vbCrLf & vbCrLf & "V�r opm�rkom p�, at du ikke kan kontakte personen ved at svare direkte p� denne email. Kontakt skal ske via de informationer, der er inkluderet ovenfor." & vbCrLF & vbCrLf & vbCrLf & "Venlig hilsen" & vbCrLf & "usedbooks.dk" & vbCrLf & "http://usedbooks.dk" 
	
	dim strSubject
	strSubject = "[usedbooks.dk] Ang. " & Title

		With msg
			.Subject = strSubject
			.Body = body
			.Sender = "support@usedbooks.dk"
			.ReplyTo = "emailrobot@usedbooks.dk"
			.SenderName = "Emailrobot - usedbooks.dk"
			.AddRecipient Email
			.ServerAddress = Application("smtpserver")
			.Execute
			'.nq
		End With
	
	Set mail = Nothing
	call updateDB(strId,formName,formPhone,formMobilePhone,formEmail,formMessage)
end sub

private sub updateDB(strBookID,strName,strPhone,strMobile,strEmail,strMsg)
	
	strName = Replace(strName,"'","''")
	if not Len(strPhone) = 0 then strPhone = Replace(strPhone,"'","''")
	if not Len(strMobile) = 0 then strMobile = Replace(strMobile,"'","''")
	if not Len(strEmail) = 0 then strEmail = Replace(strEmail,"'","''")
	strMsg = Replace(strMsg,"'","''")
	
	dim strSql ' as string
	strSql = "INSERT INTO ubdk_bookcontactlog (bookId,strId,strMsg,strName"
	
	if Len(strPhone) > 0 then strSql = strSql & ",strPhone"
	if Len(strMobile) > 0 then strSql = strSql & ",strMobile"
	if Len(strEmail) > 0 then strSql = strSql & ",strEmail"
	
	dim oGuid ' as Scriptlet..TypeLib
	dim strGuid ' as string
	set oGuid = Server.CreateObject("Scriptlet.TypeLib")
	strGuid = Left(Trim(oGuid.GUID),38)
	strSql = strSQL & ",strIP,dtNow) VALUES ('" & strBookId & "','" & strGuid & "','" & strMsg & "','" & strName & "'"
	
	if Len(strPhone) > 0 then strSql = strSql & ",'" & strPhone & "'"
	if Len(strMobile) > 0 then strSql = strSql & ",'" & strMobile & "'"
	if Len(strEmail) > 0 then strSql = strSql & ",'" & strEmail & "'"
	
	strSql = strSql & ",'" & Request.ServerVariables("REMOTE_ADDR") & "',Now())"
	on Error Resume Next
		oCon.Execute(strSql)
	On Error goto 0

end sub

%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>