<h1 class="content-indent">Resultat</h1>
<h3 class="content-indent">Du har s�gt efter <% if Len(Request.Querystring("query")) > 0 then Response.Write "&quot;" & Request.Querystring("query") & "&quot;" else Response.Write "alle b�ger" end if%><% 

if Request.QueryString("courses") <> "" then
	Response.Write " i kurset " & Request.QueryString("courses")
end if

%></h3>

<p class="content-indent"><% = strMessage %></p>
<table width="560" class="result">
	<tr class="result">
		<td>Titel</td><td>Prisid�</td><td>Udgivelses�r</td><td>Detaljer</td>
	</tr>
	<%  dim j
		dim strClass
		j = 0
		
		if bQueryDb then
			do while not oRsBooks.EOF
				if strClass = "lgray" then strClass = "dgray" else strClass = "lgray" end if
	%>
	<tr cellspacing="0" class="<% = strClass %>">
		<td class="<% = strClass %>"><img src="/images/<% = trim(oRsBooks("buyORsell")) %>.jpg" width="15" height="15" alt="Denne bog <% if trim(oRsBooks("buyORsell")) = "k" then Response.Write "�nskes k�bt" else Response.Write "er til salg" end if %>" />
			<% 
				if Len(oRsBooks("strBookTitle")) > 50 then
					Response.Write Left(oRsBooks("strBookTitle"),47) & " ..."
				else
					Response.Write oRsBooks("strBookTitle")
				end if
			%>
		</td>
		<td class="<% = strClass %>"><% = REplace(FormatCurrency(oRsBooks("intPrice"),2)," ","&nbsp;") %></td>
		<td class="<% = strClass %>" style="text-align: center;"><% if CInt(oRsBooks("intYearOfPublication")) = 0 then Response.Write "* intet �r *" else Response.Write oRsBooks("intYearOfPublication") end if%></td>
		<td class="<% = strClass %>"><form action="/books/default.asp?id=<% = Server.URLEncode(oRsBooks("bookId")) %>&amp;checksum=<% = md5(application("initVEctor") & oRsBooks("bookId"))%>" method="post"><input src="/images/detaljer.jpg" type="image" /></form></td>
	</tr>
	<%
			j = j + 1
			oRsBooks.MoveNext
			loop
		end if
	%>
	<%
		if j = 0 then
	%>
	<tr>
		<td colspan="5">... din s�gning returnerede desv�rre ingen resultater</td>
	</tr>
	<%
		end if
	%>
</table>
<% if j = 0 then %>
<br/>
<div class="content-indent">
	Der kan v�re flere �rsager til, at din s�gning ikke gav noget resultat.
	Det kan selvf�lgelig skyldes, at bogen ikke findes i vores database. Det
	kan dog ogs� skyldes, at din s�gning er for specifik. Hvis du fx har s�gt
	efter &quot;Matematisk Analyse 3&quot;, kan du opn� flere resultater, hvis
	du i stedet s�ger efter &quot;Analyse&quot;<br/>
	<br/>
	Hvis du har s�gt efter b�ger i et specifikt kursus, s� kan du igen risikere,
	at en ejer af en af b�gerne fra kurset ikke har registreret bogen som
	v�rende til et specifikt kursus. Derfor kan du i stedet pr�ve at s�ge efter
	uddrag af titlen eller efter efternavnet p� bogens forfatter.
</div>
<br/>
<div class="content-indent">
	Hvis du ikke har fundet den bog du s�gte, s� husk p�,
	at du kan k�be alle studieb�ger p� DTU i Polyteknisk Boghandel.
</div>
<br/>
<p style="text-align: right;">
	<a href="http://www.polyteknisk.dk"><img src="/images/logomargin_f5f5f5-180degrees.png" alt="Polyteknisk Boghandel har alle b�ger til stort set alle fag p� DTU" width="202" height="70" /></a>
</p>
<% end if %>
<p class="content-indent" style="text-align:center;">
<script type="text/javascript" src="http://impdk.tradedoubler.com/imp/pool/js/10472/552765"></script>
</p>