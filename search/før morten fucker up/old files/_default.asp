<%@Language="VBScript"%>
<%
option explicit
%>
<!-- #include virtual="/utilities/md5.asp" -->
<%
'************************************************************
' d. 15-11-2002 kl 14.03: Taget i brug af Jesper Stocholm
'
'************************************************************

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.CacheControl = "no-cache"
Response.AddHeader "pragma","no-cache"
%>
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
	dim oRsBooks ' as ADODB.Recordset
	set oRsBooks = Server.CreateObject("ADODB.Recordset")

	dim strQuery			' as string
	dim strCourses			' as string
	dim strSearchCategory	' as string
	dim strMessage			' as string
	dim bQueryDb			' as boolean
	
	strQuery = null
	strSearchCategory = null
	strCourses = null
	
	if Request.QueryString("query") <> "" then strQuery = Replace(Request.QueryString("query"),"'","''")
	if Request.QueryString("search") <> "" then strSearchCategory = Request.QueryString("search")
	if Request.QueryString("courses") <> "" then strCourses = Replace(Request.QueryString("courses"),"'","''")
	
	'response.End
	
	if isnull(strCourses) AND (isnull(strQuery) OR len(strQuery) < 4) then
		strMessage = "Du skal enten v�lge et fag eller angive et s�gekriterium p� mindst 4 tegn"
		bQueryDb = false
	else
		'if not isnull(strQuerystrQuery = Replace(strQuery,"'","''")
		'if not isnull(strCourses) then strCourses = Replace(strCourses,"'","''") end if
		'response.Write instr(strQuery,"-")
		'response.Write lcase(strSearchCategory)
		if instr(strQuery,"-") > 0 AND lcase(strSearchCategory) = "isbn" then
			strMessage = "Hvis du s�ger efter et ISBN-nummer, s� skal det indtastes uden bindestreger."
			'Response.Write strMessage
			bQueryDb = false
		else
			strMessage = "Resultatet af din s�gning er :"
			bQueryDb = true
			strSQL = "SELECT buyORsell, bookId,strBookTitle,dblPrice,intYearOfPublication,strCourseId,ISBN FROM viewGetTotalResults WHERE "
			SELECT CASE strSearchCategory
				CASE null
					'strSQL = strSQL & "(ISBN LIKE '*" & strQuery & "*' OR strBookTitle LIKE '*" & strQuery & "*' OR strAuthorName LIKE '*" & strQuery & "*')"
				CASE "isbn"
					strSQL = strSQL & "ISBN LIKE '%" & trim(strQuery) & "%'"
				CASE "title"
					strSQL = strSQL & "strBookTitle LIKE '%" & strQuery & "%'"
				CASE "author"
					strSQL = strSQL & "strAuthorName LIKE '%" & strQuery & "%'"
				CASE "all"
					strSQL = strSQL & "(ISBN LIKE '%" & strQuery & "%' OR strBookTitle LIKE '%" & strQuery & "%' OR strAuthorName LIKE '%" & strQuery & "%')"
				CASE ELSE
					strSQL = strSQL & "(ISBN LIKE '%" & strQuery & "%' OR strBookTitle LIKE '%" & strQuery & "%' OR strAuthorName LIKE '%" & strQuery & "%')"
			END SELECT
	
			if not isnull(strCourses) then
				strSQL = strSQL & " AND strCourseId = '" & strCourses & "' "		
			end if
	
			strSQL = strSQL & " AND tintActive = 1 ORDER by buyORsell DESC, strBookTitle ASC, intYearOfPublication DESC, dblPrice ASC"
			'strSQL = "SELECT * FROM books" ' WHERE strBookTitle LIKE '*" & "Mekanik" & "*';" 'WHERE strBookTitle LIKE '*Mekanik*'"
			'strSQL = "SELECT * FROM books WHERE strBookTitle LIKE '*" & "Mekanik" & "*';"
			'strSQL = "SELECT buyORsell, bookId,strBookTitle,dblPrice,intYearOfPublication,strCourseId,ISBN FROM viewGetTotalResults WHERE strBookTitle LIKE '%Mekanik%' ORDER by buyORsell DESC, intYearOfPublication DESC, dblPrice ASC"
			
			with oRsBooks
				.ActiveConnection = oCon
				.CursorLocation =	3 ' adUseClient
				.CursorType =		0 ' adOpenForwardOnly
				.LockType =			1 ' adLockReadOnly
				.Source =			strSQL
				.Open
			end with
		
			'set oRsBooks = oCon.Execute(strSQL)
			'Response.Write strSQL & "<br>"
		end if
		
		'call queryDatabaseForResults(Request.QueryString("query"),Request.QueryString("search"),Request.QueryString("courses"),oCon)
	end if
		
	'public sub queryDatabaseForResults(strQuery,strSearchCategory,strCourses,oCon)

	'end sub

	'call queryDatabaseForResults(Request.QueryString("query"),Request.QueryString("search"),Request.QueryString("courses"),oCon)

%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
				</td>
				<td class="leftline">
					<!-- #include virtual="/search/content.asp" -->
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>

<%
oCon.Close
set oCon = nothing
%>