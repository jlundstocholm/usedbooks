<h1 class="content-indent">Resultat</h1>
<h3 class="content-indent">Du har s�gt efter &quot;<% = Request.Querystring("query") %>&quot;<% 

if Request.QueryString("courses") <> "" then
	Response.Write " i kurset " & Request.QueryString("courses")
end if

%></h3>

<p class="content-indent"><% = strMessage %></p>
<table width="560" class="result">
	<tr class="result">
		<td>Titel</td><td>Prisid�</td><td>Udgivelses�r</td><td>Detaljer</td>
	</tr>
	<%  dim j
		dim strClass
		j = 0
		
		if bQueryDb then
			do while not oRsBooks.EOF
				if strClass = "lgray" then strClass = "dgray" else strClass = "lgray" end if
	%>
	<tr cellspacing="0" class="<% = strClass %>">
		<td class="<% = strClass %>"><img src="/images/<% = trim(oRsBooks("buyORsell")) %>.jpg" width="15" height="15" alt="Denne bog <% if trim(oRsBooks("buyORsell")) = "k" then Response.Write "�nskes k�bt" else Response.Write "er til salg" end if %>" />
			<% = oRsBooks("strBookTitle") %></td>
		<td class="<% = strClass %>"><% = FormatCurrency(oRsBooks("dblPrice"),2) %></td>
		<td class="<% = strClass %>"><% if CInt(oRsBooks("intYearOfPublication")) = 0 then Response.Write "* intet �r *" else Response.Write oRsBooks("intYearOfPublication") end if%></td>
		<td class="<% = strClass %>"><a href="/books/?id=<% = oRsBooks("bookId") %>&amp;checksum=<% = md5(application("initVEctor") & oRsBooks("bookId"))%>"><img src="/images/detaljer.jpg" alt="Se detaljer om den aktuelle bog" width="47" height="15" /></a></td>
	</tr>
	<%
			j = j + 1
			oRsBooks.MoveNext
			loop
		end if
	%>
	<%
		if j = 0 then
	%>
	<tr>
		<td colspan="5">... din s�gning returnerede desv�rre ingen resultater</td>
	</tr>
	<%
		end if
	%>
</table>
<% if j = 0 then %>
<br/>
<div class="content-indent">
	Der kan v�re flere �rsager til, at din s�gning ikke gav noget resultat.
	Det kan selvf�lgelig skyldes, at bogen ikke findes i vores database. Det
	kan dog ogs� skyldes, at din s�gning er for specifik. Hvis du fx har s�gt
	efter &quot;Matematisk Analyse 3&quot;, kan du opn� flere resultater, hvis
	du i stedet s�ger efter &quot;Analyse&quot;<br/>
	<br/>
	Hvis du har s�gt efter b�ger i et specifikt kursus, s� kan du igen risikere,
	at en ejer af en af b�gerne fra kurset ikke har registreret bogen som
	v�rende til et specifikt kursus. Derfor kan du i stedet pr�ve at s�ge efter
	uddrag af titlen eller efter efternavnet p� bogens forfatter.
</div>
<div class="content-indent">
	<br/>
	<table >
		<tr>
			<td>
				<table >
					<tr>
						<td >
							<form method="get" action="http://www.amazon.co.uk/exec/obidos/external-search" >
								<div>
									<input type="hidden" name="mode" value="books" />
									Du kan ogs� v�lge at s�ge efter din bog p� amazon.co.uk :<br/>
									<input type="text" name="keyword" size="20" value="<% = Request.QueryString("query") %>" />
									<input type="hidden" name="tag" value="useddkkobogsa-21"/>
									<input type="image" value="Go" name="Go" src="/images/pdm-search-go-btn.gif" />
								</div>
							</form>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<a href="http://www.amazon.co.uk/exec/obidos/redirect-home?tag=useddkkobogsa-21&amp;site=amazon"><img src="/images/uk_searchbox.jpg" alt="In Association with Amazon.co.uk"/></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<% end if %>