<%@Language="VBScript"%>
<%
option explicit
Response.Expires = 1440
Response.ContentType = "text/css"
%>
body {
	font-family: Arial,Sans-Serif;
	font-size:9pt;
	margin-right: 1pt;
	margin-left: 1pt;
	background: url("/images/backg.gif");
}

img {
	border-width: 0px;
}

form {
	display: inline;
}

table.box {
	background-color: #c3c3c3;
	color: #000000;
}

table.adv {
	background-color: #c3c3c3;
	color: #000000;
	margin-right: auto;
	margin-left: auto;
}

a.box {
	color: #000000;
	background: #c3c3c3;
	font-weight: bold;
	text-decoration: none;
}

a.box:active {
	color: #000000;
	background: #c3c3c3;
	font-weight: bold;
	text-decoration: none;
}

a.box:link {
	color: #000000;
	background: #c3c3c3;
	font-weight: bold;
	text-decoration: none;
}

a.box:visited {
	color: #000000;
	background: #c3c3c3;
	font-weight: bold;
	text-decoration: none;
}

a.box:hover {
	color: #ffffff;
	background: #c3c3c3;
	font-weight: bold;
	text-decoration: none;
}

a.contact {
	color: #000000;
	background: #dcdcdc;
	font-weight: bold;
	text-decoration: none;
}

a.contact:active {
	color: #000000;
	background: #dcdcdc;
	font-weight: bold;
	text-decoration: none;
}

a.contact:link {
	color: #000000;
	background: #dcdcdc;
	font-weight: bold;
	text-decoration: none;
}

a.contact:visited {
	color: #000000;
	background: #dcdcdc;
	font-weight: bold;
	text-decoration: none;
}

a.contact:hover {
	color: #000000;
	background: #dcdcdc;
	font-weight: bold;
	text-decoration: underline;
}

table.result {
	margin-left: auto;
	margin-right: auto;
	border-left: black 1px dashed;
	border-right: black 1px dashed;
	border-top: black 1px solid;
	border-bottom: black 1px solid;
	border-collapse: collapse;
}

table.contact {
	margin-left: auto;
	margin-right: 5pt;
	margin-top: 5pt;
	border-left: black 1px dashed;
	border-right: black 1px dashed;
	border-top: black 1px dashed;
	border-bottom: black 1px dashed;
	border-collapse: collapse;
	background: #dcdcdc;
	color: #000000;
	font-size: 8pt;
}

table.formular {
	margin-left: 15pt;
	margin-right: auto;
	margin-top: 5pt;
	border-left: black 1px dashed;
	border-right: black 1px dashed;
	border-top: black 1px dashed;
	border-bottom: black 1px dashed;
	border-collapse: collapse;
	background: #dcdcdc;
	color: #000000;
	font-size: 8pt;
}

tr.contact {
	background: #EFEFEF;
	color: #000000;
}

td {
	vertical-align: top;
}

img.result {
	margin-left: auto;
	margin-right: 5pt;
}

td.box {
	width: 150px;
	vertical-align: top;
}

td.logoelement {
	border-left: 0px;
	border-top: 0px;
	padding-top: 0px;
	padding-left: 0px;
}

.dgray {
	background: #EFEFEF;
	color: #000000;
}

.lgray {
	background: #dcdcdc;
	color: #000000;
}

tr.result {
	background: #7ba3de;
	color: #000000;
}

.gray {
	background-color: #c3c3c3;
	color: #000000;
}

a {
	color: #000000;
	background: #f5f5f5;
	font-weight: bold;
	text-decoration: underline;
}
a:link {
	color: #000000;
	background: #f5f5f5;
	font-weight: bold;
	text-decoration:none;
}
a:visited {
	color: #000000;
	background: #f5f5f5;
	font-weight: bold;
	text-decoration: none;
}
a:active {
	color: #000000;
	background: #f5f5f5;
	font-weight: bold;
	text-decoration: none;
}
a:hover {
	color: #000000;
	background: #f5f5f5;
	font-weight: bold;
	text-decoration: underline;
}

.headertable {
	background-color: #ffffff;
	color: #000000;
	BORDER-BOTTOM: black 1px solid;
	margin-left: auto;
	margin-right: auto;
	border-left: 0px;
	border-top: 0px;
	padding-top: 0px;
	padding-left: 0px;
}

.contenttable {
	background-color: #f5f5f5;
	color: #000000;
	margin-left: auto;
	margin-right: auto;
}

.bottomline {
	BORDER-BOTTOM: black 1px solid;
}

.leftline {
	BORDER-LEFT: black 1px solid;
	vertical-align: top;
}

.content-indent {
	 LEFT: 20px;
	 POSITION: relative;
	 padding-right: 30px;
}

.asterisk-smoke {
	color: #ff0000;
	vertical-align: middle;
	background-color: #f5f5f5;
}

.asterisk {
	color: #ff0000;
	vertical-align: middle;
	background: #dcdcdc;
}

td.cell-center-vertical {
	vertical-align: middle;
}
