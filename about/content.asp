<h1>Om usedbooks.dk</h1>

<h2 class="content-indent">Form�l</h2>
    <div class="content-indent">
    Usedbooks.dk er et website der har til form�l at formidle kontakten mellem s�lgere
    og k�bere af brugte b�ger - helt GRATIS. <b>Websitet henvender sig til studerende
    ved Danmarks Tekniske Universitet</b>. Vi h�ber at dette website kan erstatte alle
    de mange k�b/salg-sedler der h�nger rundt omkring p� DTUs opslagstavler. Desuden
    h�ber vi at et website der fokuserer p� et bestemt universitet vil fungere bedre
    end de mange websites der ikke har fokus p� en afgr�nset og specifik m�lgruppe. 
    <br />
    Websitet er ikke-kommercielt og de penge der m�tte tjenes 
    p� reklamer g�r udelukkende til betaling af webhotel og generel vedligeholdelse af websitet.
    </div>
<h2 class="content-indent">Hvordan kommer man i gang?</h2>
    <div class="content-indent">
    Hvis du er p� udkig efter en bog der er til salg, s� kan du g� i gang med at
    s�ge i databasen over brugte b�ger med det samme. Brug s�gefeltet i toppen.
    �nsker du at s�tte en bog til salg eller g�re opm�rksom p� at du gerne vil
    k�be en bog, s� skal du oprette dig som bruger. Dette g�res i menuen til venstre.
    </div>
<% '<h2 class="content-indent">Tilblivelsen af dette website</h2>
    '<div class="content-indent">
'	Ideen til dette website kom fra Jesper Stocholm, og blev implementeret i 
'	kurset <a href="http://shb.dtu.dk/default.asp?kursusnr=02335&amp;soeg=ja">02335</a> af Morten Ovi og Jesper Stocholm.
 '   </div> %>