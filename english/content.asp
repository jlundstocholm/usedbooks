<h1>In English, please</h1>
<div class="content-indent">
Hi there<br/>
<br/>
[usedbooks.dk] is a free e-marketplace for selling and buying used books currently used by several houndred 
students at DTU. Because the website is in Danish we have made an English guide on 
<a href="usedbooks.pdf">how to use usedbooks.dk</a>.<br/>
<br/>
If you have any questions, do not hesitate to email us. Use the "Kontakt" form in the left menu to contact us.<br/>
<br/>
<br/>
Jesper and Morten<br/>
usedbooks.dk<br/>
</div>