<h1>Hj�lp</h1>

<h2 class="content-indent">Generelt</h2>
    <div class="content-indent">
		Hvis du oplever problemer med brugen af dette website, s� kan det skyldes at
		din browser ikke underst�tter de teknologier som er brugt p� dette website.
		Se mere om kravene p� siden <a href="/useragreement/">Vilk�r</a>.
    </div>

<h2 class="content-indent">S�gning</h2>
    <div class="content-indent">
		S�gefeltet i �verste h�jre hj�rne kan bruges p� flere m�der. 
		<ul>
			<li>
				<h3>Teksts�gning</h3>
				Ved at skrive en tekst i s�gefeltet vil denne tekst blive brugt i
				s�gningen i vores database. Som udgangspunkt er &quot;Alle felter&quot;
				markeret, hvilket betyder, at der s�ges efter teksten i felterne ISBN,
				Titel samt Forfatter. Hvis der s�ges p� &quot;analyse&quot; i feltet
				&quot;Titel&quot; vil det resultere i alle b�ger, hvis titel har ordet
				&quot;analyse&quot; i sig.<br/>
				<br/>
				Det er ikke muligt at bruge logiske operatorer, dvs. s�gestrenge 
				som &quot;analyse og kemi&quot;. I dette tilf�lde vil resultatet blive 
				de b�ger, hvis titel indeholder "analyse og kemi". En bog med titlen 
				&quot;Kemisk Analyse&quot; vil alts� ikke blive fundet ved en s�dan s�gning.<br/>
				<br/>
				Der er ingen forskel p� store og sm� bogstaver.<br/>
				<br/>
			</li>
			<li>
				<h3>Kursuss�gning</h3>
				Du kan ogs� s�ge p� et specifikt kursus p� DTU. Dette g�res ved at
				v�lge et kursus i listen under &quot;S�g&quot;-knappen. Hvis din
				browser underst�tter JavaScript, s� vil du automatisk blive
				viderestillet til resultatsiden.<br/>
				<br/>
				Skriver man f�rst en tekst der skal s�ges p� i tekstboksen, og herefter v�lger et
				af kurser fra listen, s� vil s�gningen tage hensyn til b�de s�geteksten og kurset.<br/>
				<br/>
				V�r i dette tilf�lde opm�rksom p� at der kan v�re b�ger der <i>ikke</i>
				er blevet tilknyttet et kursus (det er muligt at s�lge andre b�ger end fagb�ger).<br/><br/>
			</li>
			<li>
				<h3>Begr�nsninger</h3>
				Det er ikke muligt at s�ge p� tekststrenge der er p� mindre end 4
				bogstaver. I alle andre tilf�lde end valg af et kursus, skal der
				angives en s�gestreng i tekstfeltet.
			</li>
		</ul>
    </div>
