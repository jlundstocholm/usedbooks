<% if bShowNews then %>
	<h1>Nyheder</h1>
	<% do while not oRsNews.EOF %>
	<h2 class="content-indent"><% = Server.HTMLEncode(oRsNews("strSubject")) %></h2>
	<p class="content-indent">Opsat d. <% = FormatDateTime(oRsNews("dtCreateDate"),vblongdate) %></p>
	<div class="content-indent">
		<% = Replace(Trim(oRsNews("strMessage")),vbCrLf,"<br/>") %>
	</div>
	<% oRsNews.MoveNext
	   loop
	%>
<% else %>
	<h1>Nyheder</h1>
	<h2 class="content-indent">Der skete en fejl.</h2>
	<div class="content-indent">
		<% = strMessage %>
	</div>
<% end if %>