<%@Language="VBScript"%>
<%
option explicit

if Application("offline") then
	Response.Redirect("/operations")
end if

Response.CharSet = "iso-8859-1"
Response.ContentType = "text/html"
Response.Expires = 0
%>
<!-- #include virtual="/Utilities/md5.asp" -->
<!-- #include virtual="/include/initialdbqueries.asp" -->
<%
	dim strSqlTop ' as string
	dim strSqlNews ' as string
	'strSqlTop = "SELECT b.buyORsell, b.strBookTitle,c.strCourseId,b.intPrice,b.bookid FROM ubdk_books b LEFT JOIN ubdk_coursedetails c ON b.intCourseId = c.id WHERE b.tintActive = 1 ORDER BY b.dtInsertDate DESC LIMIT 0,5"
	strSqlTop = "SELECT b.buyORsell, b.strBookTitle,c.strCourseId,b.intPrice,b.bookid FROM ubdk_books b LEFT JOIN ubdk_coursedetails c ON b.intCourseId = c.id INNER JOIN ubdk_users u ON b.userId = u.userId WHERE b.tintActive = 1 AND u.tintActive = 1 AND u.tintStudy = " & Application("intSchoolId") & " ORDER BY b.dtInsertDate DESC LIMIT 0,5"
	
	dim oRsTop
	set oRsTop = Server.CreateObject("ADODB.Recordset")
	with oRsTop
		.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseClient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =			1 ' adReadOnly
		.Source =			strSqlTop
		.Open
	end with
	'SELECT something FROM tbl_name
    '       WHERE TO_DAYS(NOW()) - TO_DAYS(date_col) <= 7
	strSqlNews = "SELECT dtCreateDate,strGuid,strSubject FROM ubdk_news WHERE TO_DAYS(NOW()) - TO_DAYS(dtCreateDate) <= 7 ORDER BY dtCreateDate DESC"
	dim oRsNews ' as Adodb.Recordset
	set oRsNews = Server.CreateObject("ADODB.Recordset")
	with oRsNews
		.ActiveConnection =	oCon
		.CursorLocation =	3 ' adUseClient
		.CursorType =		0 ' adOpenForwardOnly
		.LockType =			1 ' adReadOnly
		.Source =			strSqlNews
		.Open	
	end with
	
	dim bShowNewsList ' as boolan
	bShowNewsList = true
	if oRsNews.RecordCount > 0 then
		bShowNewsList = true
	else
		bShowNewsList = false
	end if
	
	
%>
<!-- #include virtual="/include/headerdeclaration.asp" -->
<!-- #include virtual="/include/toptable.asp" -->
		<table width="760" class="contenttable" id="Table2">
			<tr>
				<td class="box">
					<!-- #include virtual="/include/leftmenu.asp" -->
					<span>unoeuro.com</span>
				</td>
				<td class="leftline">
					<!-- #include file="content.asp" -->
				</td>
			</tr>
			<!--
			<tr>
				<td></td>
				<td align="right">&nbsp;</td>
			</tr>
			-->
		</table>
	</body>
</html>
<%
' cleaning up
oCon.Close
set oCon = nothing
set oRs = nothing
%>