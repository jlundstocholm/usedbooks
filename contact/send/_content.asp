<%

Dim mail, body
Set mail = Server.CreateObject("JMail.SMTPMail")

body = Request.Form("kommentar")

With mail
	.Subject = "[usedbooks.dk] Feedback fra kontakt-formular"
	.Body = body
	.Sender = Request.Form("email")
	'.Sender = "kontakt@usedbooks.dk"
	.SenderName = Request.Form("navn")
	.AddRecipient "kontakt@usedbooks.dk"
	.ServerAddress = "smtp.unoeuro.com"
	.Execute
End With

Set mail = Nothing

%>

<h1 class="content-indent">E-mail afsendt!</h1>
<div class="content-indent">
Tak for din e-mail. Den vil blive besvaret hurtigst muligt (ca. 1-2 dage).<br/><br/>
Venlig hilsen<br/>
usedbooks.dk</div>