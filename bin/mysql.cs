using System;
using System.Data;
using System.Web.UI;
using Microsoft.Data.Odbc;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Main assembly for usedbooks.dk")]
[assembly: AssemblyDescription("Handles database communication and access")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("[usedbooks.dk]")]
[assembly: AssemblyProduct("")]
//[assembly: AssemblyCopyright("None - free to use for everybody")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("1.1.*")]

// csc /out:dbOdbc.dll 
// /r:System.web.dll,System.Data.dll,Microsoft.Data.Odbc.dll /t:library 
// dbOdbc.cs

namespace usedbooks
{
	public class dbmySql : Page
	{
			public OdbcDataReader getData(string strSQL) 
			{
				OdbcConnection oCon;
				OdbcCommand oCmd = new OdbcCommand();
				oCon = new OdbcConnection("driver={MySQL};server=mysql.jirty.dk;uid=asp-faq01;pwd=rlisg;database=aspfaq;OPTION=17923");
				oCon.Open();
				oCmd.Connection = oCon;
				oCmd.CommandText = strSQL;
				OdbcDataReader result = oCmd.ExecuteReader(CommandBehavior.CloseConnection);
				OdbcConnection.ReleaseObjectPool();
				return result;
			}

			public int updateData(string strSQL) {
				OdbcConnection oCon;
				OdbcCommand oCmd = new OdbcCommand();
				oCon = new OdbcConnection("driver={MySQL};server=mysql.jirty.dk;uid=asp-faq01;pwd=rlisg;database=aspfaq;OPTION=17923");
				oCon.Open();
				oCmd.Connection = oCon;
				oCmd.CommandText = strSQL;
				int result = Convert.ToInt32(oCmd.ExecuteNonQuery());
				oCon.Close();
				OdbcConnection.ReleaseObjectPool();
				return result;
			}

			public int insertData(string strSQL) 
			{
				OdbcConnection oCon;
				OdbcCommand oCmd = new OdbcCommand();
				oCon = new OdbcConnection("driver={MySQL};server=mysql.jirty.dk;uid=asp-faq01;pwd=rlisg;database=aspfaq;OPTION=17923");
				oCon.Open();
				oCmd.Connection = oCon;
				oCmd.CommandText = strSQL;
				int result = Convert.ToInt32(oCmd.ExecuteNonQuery());
				oCon.Close();
				OdbcConnection.ReleaseObjectPool();
				return result;
			}

			public int deleteData(string strSQL) 
			{
				OdbcConnection oCon;
				OdbcCommand oCmd = new OdbcCommand();
				oCon = new OdbcConnection("driver={MySQL};server=mysql.jirty.dk;uid=asp-faq01;pwd=rlisg;database=asp-faq;OPTION=17923");
				oCon.Open();
				oCmd.Connection = oCon;
				oCmd.CommandText = strSQL;
				int result = Convert.ToInt32(oCmd.ExecuteNonQuery());
				oCon.Close();
				OdbcConnection.ReleaseObjectPool();
				return result;
			}

			public int getScalarInt(string strSQL) 
			{
				OdbcConnection oCon;
				OdbcCommand oCmd = new OdbcCommand();
				oCon = new OdbcConnection("driver={MySQL};server=mysql.jirty.dk;uid=asp-faq01;pwd=rlisg;database=aspfaq;OPTION=17923");
				oCon.Open();
				oCmd.Connection = oCon;
				oCmd.CommandText = strSQL;
				int result = Convert.ToInt32(oCmd.ExecuteScalar());
				oCon.Close();
				OdbcConnection.ReleaseObjectPool();
				return result;
			}
	}
}