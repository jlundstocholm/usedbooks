using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using Microsoft.Data.Odbc;
using aspfaq;

[assembly: AssemblyTitle("Control assembly : asp-faq.dk")]
[assembly: AssemblyDescription("Contains all code for resuable controls on asp-faq.dk")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("asp-faq.dk")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("None - free to use for everybody")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("0.9.*")]

namespace aspfaq.controls
{
	/// <summary>
	/// Summary description for controls.
	/// </summary>
	public class Categories : Control 
	{
		private string CssTable;
		private string CssTableHeader;
		private string CssTableRow;
		private string CssLink;

		public virtual string cssTable
		{
			get 
			{
				return CssTable;
			}

			set 
			{
				CssTable = value;
			}

		}


		public virtual string cssTableHeader
		{
			get 
			{
				return CssTableHeader;
			}

			set 
			{
				CssTableHeader = value;
			}

		}

		public virtual string cssTableRow
		{
			get 
			{
				return CssTableRow;
			}

			set 
			{
				CssTableRow = value;
			}

		}

		public virtual string cssLink
		{
			get 
			{
				return CssLink;
			}

			set 
			{
				CssLink = value;
			}

		}

		protected override void Render(HtmlTextWriter writer)
		{
			StringBuilder sb = new StringBuilder();

			// generates HTML for <table>-element
			char cApos = Convert.ToChar(34);
			sb.Append("<table");
				sb.Append(" width=");sb.Append(cApos);sb.Append("100%");sb.Append(cApos);
				sb.Append(" border=");sb.Append(cApos);sb.Append("0");sb.Append(cApos);
				sb.Append(" cellspacing=");sb.Append(cApos);sb.Append("0");sb.Append(cApos);
				sb.Append(" cellpadding=");sb.Append(cApos);sb.Append("3");sb.Append(cApos);
			if (CssTable != null) 
			{
				sb.Append(" class=");sb.Append(cApos);sb.Append(CssTable);sb.Append(cApos);
			}
			sb.Append(">");
			
			// generates HTML for header of table
			sb.Append("<tr><td");
			if (CssTableHeader != null)
			{
				sb.Append(" class=");sb.Append(cApos);sb.Append(CssTableHeader);sb.Append(cApos);
			}
			sb.Append(">Kategorier</td></tr>");


			// generates HTML for the normal rows in table including the links
			dbmySQL oDb = new dbmySQL();
			string strSQL = "SELECT c.id,c.strCategoryName, COUNT(f.id) AS CountOfArticles FROM deiwsaspcat c LEFT JOIN deiwsaspfaq f ON f.intCategoryID = c.id WHERE f.tintApproved IS NULL OR f.tintApproved = 1 GROUP BY c.id,c.strCategoryName ORDER BY 2 ASC";
			OdbcDataReader oData = oDb.getData(strSQL);
			
			while(oData.Read()) 
			{
				sb.Append("<tr><td");
				if (CssTableRow != null) 
				{
					sb.Append(" class=");sb.Append(cApos);sb.Append(CssTableRow);sb.Append(cApos);
				}			
				sb.Append("><a");
				if (CssLink != null) 
				{
					sb.Append(" class=");sb.Append(cApos);sb.Append(CssLink);sb.Append(cApos);
				}
				sb.Append(" href=");sb.Append(cApos);sb.Append("/search/?query=&amp;category=");sb.Append(oData.GetInt32(0).ToString());sb.Append(cApos);sb.Append(" title=");sb.Append(cApos);sb.Append("S�g efter artikler i kategorien ");sb.Append(oData.GetString(1).Trim());sb.Append(cApos);
				sb.Append(">");sb.Append(oData.GetString(1).Trim());sb.Append("</a> (");sb.Append(oData.GetInt32(2).ToString());sb.Append(")");
				sb.Append("</td></tr>");
			}
			sb.Append("</table>");
			writer.Write(sb.ToString());
		}
	}

	public class Login : Control, INamingContainer
	{
		public string EmailValue 
		{
			get
			{
				this.EnsureChildControls();
				return ((TextBox)Controls[1]).Text;
			}
		}

		public string PasswordValueSHA1
		{
			get 
			{
				this.EnsureChildControls();
				return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(((TextBox)Controls[3]).Text,"SHA1");
				//return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(((TextBox)Controls[1]).Text,"SHA1");//.ToString();
			}
		}

		protected override void CreateChildControls() 
		{
			//initialize and render controls

			//control 0
			this.Controls.Add(new LiteralControl(@"<table class=""""><tr class=""""><td class="""">Email:<br/>"));
			TextBox tEmail = new TextBox();
			tEmail.CssClass = "test";
			tEmail.ID = "tEmail";
			tEmail.Columns = 15;

			//control 1
			this.Controls.Add(tEmail);
			
			//control 2
			this.Controls.Add(new LiteralControl("<br/>Password:<br/>"));

			TextBox tPassword = new TextBox();
			tPassword.CssClass = "test";
			tPassword.ID = "tPassword";
			tPassword.TextMode = TextBoxMode.Password;
			tPassword.Columns = 10;

			//control 3
			this.Controls.Add(tPassword);
			//this.Controls.Add(new LiteralControl("<br/>"));

			Button btLogin = new Button();
			btLogin.ID = "btLogin";
			btLogin.Text = "Login";
			btLogin.Click += new System.EventHandler(this.btLogin_Click);
			//control 4
			this.Controls.Add(btLogin);

			//control 5
			this.Controls.Add(new LiteralControl("<br/>"));

			//Label lStatus = new Label();
			//lStatus.Visible = false;

			//control 6
			this.Controls.Add(new LiteralControl(@"<table border=""0"" cellspacing=""0"" cellpadding=""3"" class=""box""><tr><td class=""bottomline; headings"">Fejl</td></tr><tr><td class=""menu-content"">Du findes ikke i vores system. Mener du ikke, at det er korrekt, s� skriv til os. p� adressen <a href=""mailto:adm@asp-faq.dk"">adm@asp-faq.dk</a></td></tr></table>"));
			
			//this.Controls.Add(lStatus);

			//control 7
			this.Controls.Add(new LiteralControl(@"</td></tr><tr class=""""><td class=""""><a class="""" href=""/admin/createProfile.aspx/"">Opret</a>&nbsp;/&nbsp;<a class="""" href=""/admin/retrievePswd.aspx/"">Glemt password</a></td></tr></table>"));
			
			//control 8
			this.Controls.Add(new LiteralControl(@"<table border=""0"" width=""100%"" cellspacing=""0"" cellpadding=""3"" class=""box""><tr><td class=""bottomline; headings"">Admin</td></tr><tr><td class=""menu-content""><a href=""/admin/addArticle.aspx"">Tilf�j artikel</a></td></tr><tr><td class=""menu-content""><a href=""/admin/admin.aspx"">Profil</a></td></tr><tr><td class=""menu-content""><a href=""/admin/logout.aspx"">Logout</a></td></tr></table>"));

			int i;
//			for (i = 0;i < Controls.Count;i++) 
//			{
//				Controls[i].Visible = false;
//			}

			if(System.Web.HttpContext.Current.Session["logonValid"] == null) 
			{
				for (i=0;i<Controls.Count - 1;i++) 
				{
					Controls[i].Visible = true;
				}
				Controls[6].Visible = false;
				Controls[8].Visible = false;
			}
			else 
			{
				for (i=0;i<Controls.Count - 1;i++) 
				{
					Controls[i].Visible = false;
				}
				Controls[8].Visible = true;
			}

		}

		private void btLogin_Click(object sender, System.EventArgs e)
		{
			string strSqlLogin = "SELECT intAuthorId FROM deiwsaspauthor WHERE strEmail = '" + this.EmailValue + "' AND strPasswordHashSHA1 = '" + this.PasswordValueSHA1 + "'";
			dbmySQL oDb = new dbmySQL();
			OdbcDataReader oData = oDb.getData(strSqlLogin);
			int intId = 0;
			while(oData.Read()) 
			{
				intId = oData.GetInt32(0);
			}
			

			//string msg = "";
			int i;
			if (intId == 0) 
			{
//				((Label)Controls[6]).Visible = true;
//				msg = "Du findes ikke i vores system. Mener du ikke, at det er korrekt, s� skriv til os. p� adressen adm@asp-faq.dk";
//				((Label)Controls[6]).Text = msg;
				for (i=0;i<Controls.Count;i++) 
				{
					Controls[i].Visible = false;
				}
				Controls[6].Visible = true;
			}
			else 
			{
				System.Web.HttpContext.Current.Session["logonValid"] = true;
				System.Web.HttpContext.Current.Session["strEmail"] = this.EmailValue;
				//((Literal)Controls[8]).Visible = true;
				for (i=0;i<Controls.Count-1;i++) 
				{
					Controls[i].Visible = false;
				}
				Controls[8].Visible = true;
			}
			
		}					
	}

	public class StatPanel : Control 
	{
		protected override void Render(HtmlTextWriter writer) 
		{
			int iArticles = new int();
			int iArticleViews = new int();
			StringBuilder sb = new StringBuilder();
			dbmySQL oDb = new dbmySQL();
			string strSql = "SELECT COUNT(id) FROM deiwsaspfaq WHERE tintApproved = 1";
			OdbcDataReader oData = oDb.getData(strSql);
			while(oData.Read()) 
			{
				iArticles = oData.GetInt32(0);
			}

			strSql = "SELECT SUM(intArticleViews) FROM deiwsaspfaq";
			oData = oDb.getData(strSql);
			while(oData.Read()) 
			{
				iArticleViews = oData.GetInt32(0);
			}

			sb.Append(@"<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""3"" class=""box"">");
				sb.Append("<tr>");
					sb.Append(@"<td class=""menu-content"">");sb.Append(iArticles);sb.Append(" artikler</td>");
				sb.Append("</tr>");
				sb.Append("<tr>");
					sb.Append(@"<td class=""menu-content"">");sb.Append(iArticleViews);sb.Append(" artikelvisninger</td>");
				sb.Append("</tr>");
			sb.Append("</table>");

			writer.Write(sb.ToString());

		}
	}

	public class SponsorLink : Control 
	{
		protected override void Render(HtmlTextWriter writer) 
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(@"<a href=""http://www.jirty.dk""><img src=""/images/banner_125_50.gif"" alt=""asp-faq.dk er sponsoreret af jirty.dk"" height=""50"" width=""125""/></a>");
			writer.Write(sb.ToString());
		}
	}
	public class HeaderData : Control 
	{
		private string PageTitle;

		public virtual string pageTitle
		{
			get 
			{
				return PageTitle;
			}

			set 
			{
				PageTitle = value;
			}

		}

		protected override void Render(HtmlTextWriter writer) 
		{
			StringBuilder sb = new StringBuilder();
			char cApos = Convert.ToChar(34);
			sb.Append(@"<table class=""bottomline"" cellspacing=""0"" cellpadding=""4"" width=""100%"" border=""0"">");
				sb.Append(@"<tr valign=""bottom"">");
					sb.Append(@"<td width=""50%"">");
						sb.Append(@"<a title=""Tilbage til hovedsiden"" href=""/"">");
							sb.Append(@"<img height=""60"" alt=""FAQ for dk.edb.internet.webdesign.serverside.asp omhandlende Active Server Pages(ASP)"" src=""images/asp-faq.jpg"" width=""468""/>");
						sb.Append("</a>");
					sb.Append("</td>");
					sb.Append(@"<td align=""right"" width=""50%"">");
						sb.Append(@"<form action=""/search"" method=""get"">");
							sb.Append(@"<input type=""hidden"" name=""category"" />");
							sb.Append(@"<table cellspacing=""0"" cellpadding=""0"" width=""100"" border=""0"">");
								sb.Append("<tr>");
									sb.Append(@"<td class=""bold"" colspan=""2"">S�gning:</td>");
								sb.Append("</tr>");
								sb.Append("<tr>");
									sb.Append(@"<td><input class=""input-search"" type=""text"" size=""20"" name=""query"" /></td>");
									sb.Append(@"<td><input class=""submitters"" type=""submit"" value=""S�g"" /></td>");
								sb.Append("</tr>");
							sb.Append("</table>");
						sb.Append("</form>");
						sb.Append("<table>");
							sb.Append("<tr>");
								sb.Append(@"<td class=""bold"">");
									sb.Append(@"<a class=""nouline"" Title=""[Om asp-faq.dk]"" href=""/about.aspx""/>[Om asp-faq.dk]</a>");
								sb.Append("</td>");
							sb.Append("</tr>");
						sb.Append("</table>");
					sb.Append("</td>");
				sb.Append("</tr>");
			sb.Append("</table>");
			writer.Write(sb.ToString());
		}

	}

}
