<%
	dim strDisabled ' as string
	if Application("offline") then strDisabled = "disabled=""disabled"""
	if Session("logonValid") <> true then
%>
<form action="/admin/login/default.asp" method="post" id="Form2" onsubmit="return validateLogin(document.forms[1].strEmail.value,document.forms[1].strPassword.value);">
	<table width="100%" class="box" id="Table3">
		<tr>
			<td>
				E-mail:<br />
				<input <% = strDisabled %> class="white" type="text" size="10" id="strEmail" name="strEmail" /><br />
				Password:<br />
				<input <% = strDisabled %>  class="white" type="password" value="" size="10" id="strPassword" name="strPassword" /> 
				<input <% = strDisabled %>  type="submit" value="Login" /><br />
				<% if not application("offline") then %>
				<a class="box" href="/admin/createprofile/">Ny bruger?</a><br/>
				<a class="box" href="/admin/retrievepassword/">Glemt password?</a>
				<% else %>
				Ny bruger?<br/>
				Glemt password?
				<% end if%>
			</td>
		</tr>
	</table>
</form>
<%
	else
%>
<table width="100%" class="box" id="Table6">
	<tr>
		<td>Hej <% = Session("strFirstName") %></td>
	</tr>
	<tr>
		<td><a class="box" href="/admin/profile/">Brugerprofil</a></td>
	</tr>
	<tr>
		<td><a class="box" href="/admin/bookadmin/">Mine b�ger</a></td>
	</tr>
	<% if Session("tintRole") = 1 then %>
	<tr>
		<td><a class="box" href="/admin/tools/">BOfH domain</a></td>
	</tr>
	<% end if %>
	<tr>
		<td><a class="box" href="/admin/logout/">Log af</a></td>
	</tr>
</table>
<%
	end if
%>
<br />
<% if not application("offline") then %>
<table width="100%" class="box">
	<tr>
		<td><a class="box" href="/contact/" title="Kontakt usedbooks.dk">Kontakt</a></td>
	</tr>
	<tr>
		<td><a class="box" href="/useragreement/" title="Se vores politik om indsamling af personoplysninger samt systemkrav">Vilk�r</a></td>
	</tr>
	<tr>
		<td><a class="box" href="/news/" title="Se nyheder herunder de udsendte nyhedsbreve">Nyheder</a></td>
	</tr>
	<tr>
		<td><a class="box" href="/help/" title="F� detaljeret hj�lp til at optimere din s�gning p� usedbooks.dk">Hj�lp til s�gning</a></td>
	</tr>
	<tr>
		<td><a class="box" href="/about/" title="Om usedbooks.dk og folkene bag servicen.">Om usedbooks.dk</a></td>
	</tr>
		<tr>
		<td><a class="box" href="/english/" title="In English, please">English <img src="/images/uk.gif" alt="In English, please" height="12" width="16"/></a></td>
	</tr>
</table>
<% else %>
<table width="100%" class="box" id="Table1">
	<tr>
		<td>Kontakt</td>
	</tr>
	<tr>
		<td><a class="box" href="/useragreement/" title="Se vores politik om indsamling af personoplysninger samt systemkrav">Vilk�r</a></td>
	</tr>
	<tr>
		<td><a class="box" href="/news/" title="Se nyheder herunder de udsendte nyhedsbreve">Nyheder</a></td>
	</tr>
	<tr>
		<td><a class="box" href="/help/" title="F� detaljeret hj�lp til at optimere din s�gning p� usedbooks.dk">Hj�lp til s�gning</a></td>
	</tr>
	<tr>
		<td><a class="box" href="/about/" title="Om usedbooks.dk og folkene bag servicen.">Om usedbooks.dk</a></td>
	</tr>
		<tr>
		<td><a class="box" href="/english/" title="In English, please">English <img src="/images/uk.gif" alt="In English, please" height="12" width="16"/></a></td>
	</tr>
</table>
<% end if %>

<!--
<a href="http://validator.w3.org/check/referer">
	<img height="10" alt="Valid XHTML 1.0 !" src="/images/xhtml.gif" width="43" />
</a>
-->